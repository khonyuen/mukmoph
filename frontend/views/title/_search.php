<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\TitleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-title-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'TitleCode')->textInput(['maxlength' => true, 'placeholder' => 'TitleCode']) ?>

    <?= $form->field($model, 'TName')->textInput(['maxlength' => true, 'placeholder' => 'TName']) ?>

    <?= $form->field($model, 'Sex')->textInput(['maxlength' => true, 'placeholder' => 'Sex']) ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

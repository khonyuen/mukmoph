<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Title */

$this->title = 'Update Title: ' . ' ' . $model->TitleCode;
$this->params['breadcrumbs'][] = ['label' => 'Title', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->TitleCode, 'url' => ['view', 'id' => $model->TitleCode]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="title-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

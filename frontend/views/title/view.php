<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Title */

$this->title = $model->TitleCode;
$this->params['breadcrumbs'][] = ['label' => 'Title', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="title-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Title'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->TitleCode], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->TitleCode], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'TitleCode',
        'TName',
        'Sex',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerUser->totalCount){
    $gridColumnUser = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'username',
            'auth_key',
            'password_hash',
            'password_reset_token',
            'email:email',
            'status',
                        'fname',
            'lname',
            'postlevel',
            [
                'attribute' => 'dep.dep_id',
                'label' => 'Dep'
            ],
            'level',
            'tel',
            'line_id',
            'line_freq',
            'line_info',
            'lastlogin',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerUser,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-user']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('User'),
        ],
        'export' => false,
        'columns' => $gridColumnUser
    ]);
}
?>

    </div>
</div>

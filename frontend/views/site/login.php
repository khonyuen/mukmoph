<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */

/* @var $model \common\models\LoginForm */

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

$this->title = 'เข้าระบบ เพื่อจัดการข้อมูล';
$this->params['breadcrumbs'][] = [
    'label' => $this->title,
    'url' => '#',
    'class' => Yii::$app->params['separatorClass'],
];
?>
<div class="row">
    <div class="col-12">
        <div class="card card-custom gutter-b">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label"><?= $this->title ?></h3>
                </div>

            </div><?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'layout' => 'horizontal'
            ]); ?>
            <div class="card-body">

                <div class="row">
                    <div class="col-6">


                        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                        <?= $form->field($model, 'password')->passwordInput() ?>

                        <?= $form->field($model, 'rememberMe')->checkbox() ?>

                    </div>
                </div>
                <div class="row">
                    <div class="col-10 offset-2">
                        <h3 class="font-size-lg text-dark font-weight-bold mb-6">หากท่านลืมรหัสผ่าน สามารถขอรหัสผ่านใหม่ได้ <?= Html::a('ขอรหัสผ่านใหม่', ['site/request-password-reset']) ?></h3>
                        <h3 class="font-size-lg text-dark font-weight-bold mb-6">หากท่านยังไม่มี Account สามารถลงทะเบียนเพื่อขอ Account ได้ <?= Html::a('ลงทะเบียน', ['site/signup']) ?></h3>
                    </div>
                </div>


            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-2"></div>
                    <div class="col-6">
                        <button type="submit" class="btn btn-success mr-2">เข้าระบบ</button>

                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>


<?php

/* @var $this yii\web\View */

use t02\assets\OwlCarousel;

$this->title = 'สำนักงานสาธารณสุขจังหวัดมุกดาหาร';
$this->params['breadcrumbs'][] = [
    'label' => $this->title,
    'url' => '#',
    'class' => Yii::$app->params['separatorClass'],
    'encode' => false,
];

$dr = Yii::$app->assetManager->getPublishedUrl('@t02/dist');
OwlCarousel::register($this);
$image = Yii::getAlias('@web');
?>

    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Hero-->
            <div class="card card-custom overflow-hidden position-relative mb-8">
                <!--<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22800%22%20height%3D%22400%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20800%20400%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_177b3d4ef84%20text%20%7B%20fill%3A%23333%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A40pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_177b3d4ef84%22%3E%3Crect%20width%3D%22800%22%20height%3D%22400%22%20fill%3D%22%23555%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22265.1796875%22%20y%3D%22217.7609375%22%3EThird%20slide%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22800%22%20height%3D%22400%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20800%20400%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_177b3d4ef82%20text%20%7B%20fill%3A%23555%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A40pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_177b3d4ef82%22%3E%3Crect%20width%3D%22800%22%20height%3D%22400%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22274.046875%22%20y%3D%22217.7609375%22%3EFirst%20slide%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22800%22%20height%3D%22400%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20800%20400%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_177b3d4ef83%20text%20%7B%20fill%3A%23444%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A40pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_177b3d4ef83%22%3E%3Crect%20width%3D%22800%22%20height%3D%22400%22%20fill%3D%22%23666%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22235.5234375%22%20y%3D%22217.7609375%22%3ESecond%20slide%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="Third slide">
                        </div>
                    </div>
                </div>-->

                <div class="owl-carousel owl-theme">
                    <div class="item">
                        <img src="<?= $image ?>/uploads/rotations/1.jpg">
                        <div class="item-caption">
                            <!-- Applying animation to Caption using animate.css  -->
                            <!-- add class animated <animation-name> provided by animate.css  -->
                            <h3 class="animated fadeInUp">นพ.ประวิตร ศรีบุญรัตน์ <br>ประชุมเตรียมรับตรวจราชการ</h3>
                        </div>
                    </div>
                    <div class="item"><img src="<?= $image ?>/uploads/rotations/2.jpg">
                        <div class="item-caption">
                            <!-- Applying animation to Caption using animate.css  -->
                            <!-- add class animated <animation-name> provided by animate.css  -->
                            <h3 class="animated fadeInUp">ประชุมเตรียมรับตรวจราชการ</h3>
                        </div>
                    </div>
                    <div class="item"><img src="<?= $image ?>/uploads/rotations/3.jpg">
                        <div class="item-caption">
                            <!-- Applying animation to Caption using animate.css  -->
                            <!-- add class animated <animation-name> provided by animate.css  -->
                            <h3 class="animated fadeInUp">ประชุมเตรียมรับตรวจราชการ</h3>
                        </div>
                    </div>
                    <div class="item"><img src="<?= $image ?>/uploads/rotations/4.jpg">
                        <div class="item-caption">
                            <!-- Applying animation to Caption using animate.css  -->
                            <!-- add class animated <animation-name> provided by animate.css  -->
                            <h3 class="animated fadeInUp">ประชุมเตรียมรับตรวจราชการ</h3>
                        </div>
                    </div>
                </div>

            </div>
            <!--end::Hero-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->

<?php
$this->registerJs("
$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    autoplay:true,
    mergeFit:false,
    autoWidth: false,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:3,
            nav:false
        },
        1000:{
            items:3,
            nav:true,
            loop:false
        }
    }
})
");


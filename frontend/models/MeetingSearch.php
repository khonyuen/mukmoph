<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Meeting;

/**
 * frontend\models\MeetingSearch represents the model behind the search form about `common\models\Meeting`.
 */
 class MeetingSearch extends Meeting
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['meeting_id', 'meeting_type', 'meeting_status', 'created_user', 'updated_user'], 'integer'],
            [['meeting_number', 'meeting_time', 'meeting_date', 'created_at', 'updated_at','meeting_month','meeting_year'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Meeting::find()->joinWith(['meetingType']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

//        print_r($params);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        //print_r($params);

        if(($this->meeting_year=='')){
            $this->meeting_year = (date('Y')+543);
        }

        $query->andFilterWhere([
            'meeting_id' => $this->meeting_id,
            'meeting_type' => $this->meeting_type,
            'meeting_time' => $this->meeting_time,
            'meeting_month' => $this->meeting_month,
            'meeting_year' => $this->meeting_year,
            'meeting_date' => $this->meeting_date,
            'meeting_status' => $this->meeting_status,
            'created_user' => $this->created_user,
            'updated_user' => $this->updated_user,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'meeting_number', $this->meeting_number]);

        return $dataProvider;
    }
}

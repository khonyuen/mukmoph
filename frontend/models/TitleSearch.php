<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Title;

/**
 * frontend\models\TitleSearch represents the model behind the search form about `common\models\Title`.
 */
 class TitleSearch extends Title
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TitleCode', 'TName', 'Sex'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Title::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'TitleCode', $this->TitleCode])
            ->andFilterWhere(['like', 'TName', $this->TName])
            ->andFilterWhere(['like', 'Sex', $this->Sex]);

        return $dataProvider;
    }
}

<?php
return [
    'adminEmail' => 'admin@example.com',
    'bsVersion' => '4.x',
    'separator'=>" > ",
    'separatorClass'=>'text-white text-hover-white opacity-75 hover-opacity-100',
    'office'=>'สำนักงานสาธารณสุขจังหวัดมุกดาหาร'
];

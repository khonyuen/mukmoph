<?php

use yii\web\Request;

$baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl()); // replace text for url manager

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'name' => 'สำนักงานสาธารณสุขจังหวัดมุกดาหาร',
    'language' => 'TH-th',
    'aliases' => [
        '@t02' => '@common/themes/t02',
        '@source' => '@t02/source',
    ],
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log','gii'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
//        'easyAjax' => [
//            'class' => 'letsjump\easyAjax\EasyAjaxBase',
//            'customOptions' => [
//                /* refer to plugin documentation */
//            ]
//        ],
        'view'=>[
            'theme' => [
                'pathMap' => [
                    //'@app/views'=>'@common/themes/modernadmin/views'
                    //'@app/views' => '@modernadmin/views',
                    '@app/views' => '@t02/views',
                ],
            ],
        ],
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => $baseUrl, // add url manager
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'baseUrl' => $baseUrl, // add url manager
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        'assetManager' => [
            'linkAssets' => true,
            'bundles' => [
//                'yii\web\JqueryAsset' => [
//                    'js'=>[] // disable JQuery
//                ],
                'yii\bootstrap4\BootstrapPluginAsset' => [
                    'js'=>[] // disable JS Bootstrap
                ],
                'yii\bootstrap4\BootstrapAsset' => [
                    'css' => [], // disable Css Bootstrap
                ],
            ],
        ],

    ],
    'params' => $params,
    'modules'=>[
        'gridview' => [
            'class' => '\kartik\grid\Module',
            // see settings on http://demos.krajee.com/grid#module
        ],
        'datecontrol' => [
            'class' => '\kartik\datecontrol\Module',
        ],
        'meeting'=>[
            'class' => 'frontend\modules\meeting',
        ]
    ]
];

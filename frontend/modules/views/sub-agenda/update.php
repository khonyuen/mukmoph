<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SubAgenda */

$this->title = 'Update Sub Agenda: ' . ' ' . $model->sub_agenda_id;
$this->params['breadcrumbs'][] = ['label' => 'Sub Agenda', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->sub_agenda_id, 'url' => ['view', 'id' => $model->sub_agenda_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sub-agenda-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

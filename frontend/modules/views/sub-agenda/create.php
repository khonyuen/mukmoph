<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SubAgenda */

$this->title = 'Create Sub Agenda';
$this->params['breadcrumbs'][] = ['label' => 'Sub Agenda', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-agenda-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

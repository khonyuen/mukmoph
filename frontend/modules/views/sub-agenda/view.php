<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\SubAgenda */

$this->title = $model->sub_agenda_id;
$this->params['breadcrumbs'][] = ['label' => 'Sub Agenda', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sub-agenda-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Sub Agenda'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->sub_agenda_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->sub_agenda_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'sub_agenda_id',
        [
            'attribute' => 'agenda.agenda_id',
            'label' => 'Agenda',
        ],
        'agenda_topic_id',
        'detail:ntext',
        'sub_order',
        'sub_files:ntext',
        'sub_files_tmp',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>MtAgenda<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnMtAgenda = [
        'meeting_id',
        'agenda_name',
        'agenda_files',
        'agenda_files_tmp',
        'agenda_topic_type',
        [
            'attribute' => 'agendaTopic.agenda_topic_id',
            'label' => 'Agenda Topic',
        ],
        'agenda_status',
        'agenda_order',
    ];
    echo DetailView::widget([
        'model' => $model->agenda,
        'attributes' => $gridColumnMtAgenda    ]);
    ?>
    
    <div class="row">
<?php
if($providerMtSubAgendaFile->totalCount){
    $gridColumnMtSubAgendaFile = [
        ['class' => 'yii\grid\SerialColumn'],
            'file_id',
                        'file_name',
            'file_path',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMtSubAgendaFile,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-mt-sub-agenda-file']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Mt Sub Agenda File'),
        ],
        'export' => false,
        'columns' => $gridColumnMtSubAgendaFile
    ]);
}
?>

    </div>
</div>

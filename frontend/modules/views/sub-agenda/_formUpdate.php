<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\SubAgenda */
/* @var $modelAgenda common\models\Agenda */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos' => \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'MtSubAgendaFile',
        'relID' => 'mt-sub-agenda-file',
        'value' => \yii\helpers\Json::encode($model->mtSubAgendaFiles),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0,
    ],
]);
?>
<?php
$this->title = "บันทึกวาระย่อย "."(".$modelAgenda->agendaTopic->topic->topic_name.' '.$modelAgenda->agenda_name.")";
?>

<div class="agenda-form">

    <?php $form = \yii\bootstrap4\ActiveForm::begin([
        'options' => [
            'class' => 'form',
            'enctype' => 'multipart/form-data',
        ],
        'id' => 'update-sub-agenda',
        'validateOnSubmit' => true,
        'validateOnBlur' => false,
        'validateOnChange' => false,
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['validate-sub-agenda']),
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'label' => 'col-sm-2',
                'offset' => 'offset-sm-0',
                'wrapper' => 'col-sm-10',
                'error' => '',
                'hint' => '',
            ],
        ],
        'layout' => 'horizontal',
    ]);

    echo Html::activeHiddenInput($model, 'agenda_id');
    echo Html::activeHiddenInput($model, 'sub_files_tmp');
    echo Html::activeHiddenInput($model, 'agenda_topic_id');
    ?>

    <div class="card card-custom gutter-b example example-compact">
        <div class="card-header">
            <h3 class="card-title"><?= $this->title ?></h3>
        </div>
        <div class="card-body">
            <?= $form->errorSummary($model); ?>

            <?= $form->field($model, 'detail')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'sub_files[]')->widget(\kartik\widgets\FileInput::className(), [
                'language' => 'th',
                'options' => [
                    'multiple' => true,
                ],
                'pluginOptions' => [
                    'showPreview' => true,
                    'showRemove' => true,
                    'showUpload' => false,
                    'overwriteInitial' => true,
                    'autoReplace' => true,
                    //'uploadUrl' => Url::to(['meeting-responsible/create']),
                    'initialPreview' => $model->initialPreview($model->sub_files, 'sub_files', 'files'),
                    'previewFileType' => ['pdf', 'html', 'text', 'video', 'audio', 'flash', 'object','rar','zip'],
                    'purifyHtml' => true,
                    //'deleteUrl'=>Url::to(['area/deletefile','id'=>$model->area_id, 'field'=>'area_pic','fileName'=>'']),
                    'initialPreviewAsData' => true,
                    'initialPreviewConfig' => $model->initialPreview($model->sub_files, 'sub_files', 'config'),
                ],
                //''
            ]) ?>

            <?= $form->field($model, 'sub_order')->textInput(['placeholder' => '','type'=>'number']) ?>

            <div class="card-footer">
                <div class="row">
                    <div class="col-2"></div>
                    <div class="col-10">
                        <?= Html::submitButton($model->isNewRecord ? 'บันทึก' : 'แก้ไข', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        <?= Html::a('ยกเลิก', '#', ['class' => 'btn btn-danger lovClose', 'data-dismiss' => 'modal']) ?></div>
                </div>

                <?php \yii\bootstrap4\ActiveForm::end(); ?>

            </div>

        </div>
    </div>
</div>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\MeetingSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-meeting-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'meeting_id')->textInput(['placeholder' => 'Meeting']) ?>

    <?= $form->field($model, 'meeting_type')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\MtMeetingType::find()->orderBy('meeting_type_id')->asArray()->all(), 'meeting_type_id', 'meeting_type_id'),
        'options' => ['placeholder' => 'Choose Mt meeting type'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'meeting_number')->textInput(['maxlength' => true, 'placeholder' => 'Meeting Number']) ?>

    <?= $form->field($model, 'meeting_time')->widget(\kartik\datecontrol\DateControl::className(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_TIME,
        'saveFormat' => 'php:H:i:s',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Meeting Time',
                'autoclose' => true
            ]
        ]
    ]); ?>

    <?= $form->field($model, 'meeting_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Meeting Date',
                'autoclose' => true
            ]
        ],
    ]); ?>

    <?php /* echo $form->field($model, 'meeting_status')->textInput(['placeholder' => 'Meeting Status']) */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

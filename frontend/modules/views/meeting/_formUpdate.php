<?php

use common\models\MeetingType;
use common\models\Room;
use common\models\TopicType;
use kartik\widgets\DatePicker;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model common\models\Meeting */
/* @var $form yii\bootstrap4\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos' => \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'MtAgenda',
        'relID' => 'mt-agenda',
        'value' => \yii\helpers\Json::encode($model->mtAgendas),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0,
    ],
]);
/*\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos' => \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'MtAgendaTopic',
        'relID' => 'mt-agenda-topic',
        'value' => \yii\helpers\Json::encode($model->mtAgendaTopics),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0,
    ],
]);*/
?>

    <div class="meeting-form">

        <?php $form = ActiveForm::begin([
            'options' => [
                'class' => 'form',
            ],
            'id' => 'update-meeting',
            'validateOnSubmit' => true,
            'validateOnBlur' => false,
            'validateOnChange' => false,
            'enableAjaxValidation' => true,
            'validationUrl' => Url::to(['validate-meeting']),
            'fieldConfig' => [
                'horizontalCssClasses' => [
                    'label' => 'col-sm-4',
                    'offset' => 'offset-sm-0',
                    'wrapper' => 'col-sm-8',
                    'error' => '',
                    'hint' => '',
                ],
            ],
            'layout' => 'horizontal',
        ]);

        echo Html::activeHiddenInput($model, 'meeting_id');
        echo Html::activeHiddenInput($model, 'meeting_status');
        ?>
        <div class="card card-custom gutter-b example example-compact">
            <div class="card-header">
                <h3 class="card-title"><?= $this->title ?></h3>
            </div>
            <div class="card-body">
                <?= $form->errorSummary($model); ?>

                <div class="row">
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <?= $form->field($model, 'meeting_type')->inline()->radioList(MeetingType::getMeetingType()); ?>
                    </div>

                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <?= $form->field($model, 'meeting_room')->dropdownList(Room::getRoomList()); ?>
                    </div>
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <?= $form->field($model, 'meeting_month')->dropdownList($model::monthInThai()); ?>
                    </div>
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <?= $form->field($model, 'meeting_year')->textInput(); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <?= $form->field($model, 'meeting_number')->textInput(['maxlength' => true, 'placeholder' => '']) ?>
                    </div>

                    <div class="col-lg-4 col-md-4 col-xs-12"><?= $form->field($model, 'meeting_date')->widget(DatePicker::classname(), [
//                            'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
//                            'saveFormat' => 'php:Y-m-d',
//                            'displayFormat' => 'php:d-m-Y',
//                            'ajaxConversion' => true,
                            'language' => 'th',
                            'options' => [
                                'autocomplete' => "off",
                            ],
                            'pluginOptions' => [
                                'format' => 'dd-mm-yyyy',
                                'todayHighlight' => true,
                                'size' => 'xs',
                                'autoclose'=>true
                            ]
                        ]); ?></div>
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <?= $form->field($model, 'meeting_time')->textInput(); ?>
                    </div>
                </div>
                <div class="separator separator-dashed my-8"></div>
                <div class="row">
                    <!--<div class="col-sm-2">
                        <h3 class="font-size-lg text-dark font-weight-bold mb-6">เลือกวาระการประชุมที่ต้องการ : </h3>
                    </div>-->
                    <h3 class="font-size-lg text-dark font-weight-bold pl-4">เลือกวาระการประชุมที่ต้องการ : (** วาระ ระบบจะอัพเดทให้ทันทีหากมีการแก้ไข)</h3>
                    <div class="col-sm-12">
                        <?php
                        echo $form->field($model, '_agenda_topic', [
                            //'fieldConfig' => [
                            'horizontalCssClasses' => [
                                'label' => 'col-sm-0',
                                'offset' => 'offset-sm-0',
                                'wrapper' => 'col-sm-12',
                                'error' => '',
                                'hint' => '',
                            ],
                            //],
                        ])->dropdownList(TopicType::getTopicListUpdate(true,$model->meeting_id), [
                            'class' => 'dual-listbox ',
                            'id'=>'_agenda_topic_update',
                            'multiple' => 'multiple',
                            'data-available-title' => 'วาระทั้งหมด',
                            'data-selected-title' => 'วาระที่เลือก',
                            'data-add' => '&lt;i class=\'flaticon2-next\'&gt;&lt;/i&gt;',
                            'data-remove' => '&lt;i class=\'flaticon2-back\'&gt;&lt;/i&gt;',
                            'data-add-all' => '&lt;i class=\'flaticon2-fast-next\'&gt;&lt;/i&gt;',
                            'data-remove-all' => '&lt;i class=\'flaticon2-fast-back\'&gt;&lt;/i&gt;',
                        ])->label(false);
                        ?>
                        <!--<select id="kt_dual_listbox_2" class="dual-listbox" multiple="multiple" data-available-title="Source Options" data-selected-title="Destination Options" data-add="&lt;i class='flaticon2-next'&gt;&lt;/i&gt;" data-remove="&lt;i class='flaticon2-back'&gt;&lt;/i&gt;" data-add-all="&lt;i class='flaticon2-fast-next'&gt;&lt;/i&gt;" data-remove-all="&lt;i class='flaticon2-fast-back'&gt;&lt;/i&gt;">
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                            <option value="4">Four</option>
                            <option value="5">Five</option>
                            <option value="6">Six</option>
                            <option value="7">Seven</option>
                            <option value="8">Eight</option>
                            <option value="9">Nine</option>
                            <option value="10">Ten</option>
                        </select>-->
                    </div>
                </div>

                <?php
                /*$forms = [
                    [
                        'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('MtAgenda'),
                        'content' => $this->render('_formMtAgenda', [
                            'row' => \yii\helpers\ArrayHelper::toArray($model->mtAgendas),
                        ]),
                    ],
                    [
                        'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('MtAgendaTopic'),
                        'content' => $this->render('_formMtAgendaTopic', [
                            'row' => \yii\helpers\ArrayHelper::toArray($model->mtAgendaTopics),
                        ]),
                    ],
                ];*/
                /*echo kartik\tabs\TabsX::widget([
                    'items' => $forms,
                    'position' => kartik\tabs\TabsX::POS_ABOVE,
                    'encodeLabels' => false,
                    'pluginOptions' => [
                        'bordered' => true,
                        'sideways' => true,
                        'enableCache' => false,
                    ],
                ]);*/
                ?>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-2"></div>
                    <div class="col-10">
                        <?= Html::submitButton($model->isNewRecord ? 'บันทึก' : 'แก้ไข', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        <?= Html::a('ยกเลิก', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?></div>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>

<?php
//echo Yii::getAlias('@t02/dist') . "/assets/js/pages/features/miscellaneous/dual-listbox.js";
$ds = Yii::$app->assetManager->getPublishedUrl('@t02/dist');
$this->registerJsFile($ds . "/assets/js/meeting-update.js",
    [
        'position' => View::POS_END,
        'depends' => '\t02\assets\MainAsset',
    ]);
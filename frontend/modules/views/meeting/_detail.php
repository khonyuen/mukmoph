<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Meeting */

?>
<div class="meeting-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->meeting_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'meeting_id',
        [
            'attribute' => 'meetingType.meeting_type_id',
            'label' => 'Meeting Type',
        ],
        'meeting_number',
        'meeting_time',
        'meeting_date',
        'meeting_status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
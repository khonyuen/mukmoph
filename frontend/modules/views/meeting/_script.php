<?php
use yii\helpers\Url;

?>
<script>
    $('#meeting-meeting_time').timepicker({
        minuteStep: 5,
        defaultTime: '09.00',
        showSeconds: false,
        showMeridian: false,
        snapToStep: true
    });
</script>

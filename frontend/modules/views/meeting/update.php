<?php

use common\models\Meeting;
use yii\helpers\Url;
use yii\web\View;


/* @var $this yii\web\View */
/* @var $model common\models\Meeting */

//echo Yii::$app->params['separator'];
//echo Yii::$app->params['separatorClass'];

$this->title = 'แก้ไขหัวข้อการประชุม';
$this->params['breadcrumbs'][] = [
    'label' => "รายการประชุมทั้งหมด " . Yii::$app->params['separator'] . " ",
    'url' => Url::to(['/meeting/default/index']),
    'class' => Yii::$app->params['separatorClass'],
    //'encode'=>true
];

$this->params['breadcrumbs'][] = [
    'label' => $this->title,
    #'url' => '',
    'class' => Yii::$app->params['separatorClass'],
];

$this->params['toolbar'] = [
    [
        'type' => 'button',
        'label' => 'สร้างวาระ',
        'style' => 'success',
        'link' => 'javascript:;',
        'sub_button' => [
            [
                'type' => 'button',
                'label' => 'สร้างวาระ กวป', 'link' => Url::to(['/meeting/meeting/create', 'option' => '1',]),
                'text' => false,
                'icon' => 'flaticon2-add',
            ],
            [
                'type' => 'button',
                'label' => 'สร้างวาระ กบห', 'link' => Url::to(['/meeting/meeting/create', 'option' => '2']),
                'text' => false,
                'icon' => 'flaticon2-add',
            ],
        ],
    ],
];
?>
<?= $this->render('_formUpdate', [
    'model' => $model,
]) ?>

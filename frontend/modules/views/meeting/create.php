<?php

use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model common\models\Meeting */

//echo Yii::$app->params['separator'];
//echo Yii::$app->params['separatorClass'];

$this->title = 'สร้างหัวข้อการประชุม';
$this->params['breadcrumbs'][] = [
    'label' => "รายการประชุมทั้งหมด " . Yii::$app->params['separator'] . " ",
    'url' => Url::to(['/meeting/default/index']),
    'class' => Yii::$app->params['separatorClass'],
    //'encode'=>true
];

$this->params['breadcrumbs'][] = [
    'label' => $this->title,
    #'url' => '',
    'class' => Yii::$app->params['separatorClass'],
];
?>
<?= $this->render('_form', [
    'model' => $model,
]) ?>

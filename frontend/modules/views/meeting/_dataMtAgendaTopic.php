<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->mtAgendaTopics,
        'key' => 'agenda_topic_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        'agenda_topic_id',
        [
                'attribute' => 'topic.topic_id',
                'label' => 'Topic'
            ],
        'order',
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'mt-agenda-topic'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);

<div class="form-group" id="add-mt-agenda">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'MtAgenda',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'agenda_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'agenda_name' => ['type' => TabularForm::INPUT_TEXTAREA],
        'agenda_files' => ['type' => TabularForm::INPUT_TEXTAREA],
        'agenda_files_tmp' => ['type' => TabularForm::INPUT_TEXT],
        'agenda_topic_type' => [
            'label' => 'Mt topic type',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\TopicType::find()->orderBy('topic_id')->asArray()->all(), 'topic_id', 'topic_id'),
                'options' => ['placeholder' => 'Choose Mt topic type'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'agenda_status' => ['type' => TabularForm::INPUT_TEXT],
        'agenda_order' => ['type' => TabularForm::INPUT_TEXT],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowMtAgenda(' . $key . '); return false;', 'id' => 'mt-agenda-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Mt Agenda', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowMtAgenda()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>


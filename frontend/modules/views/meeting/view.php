<?php

use common\models\AgendaTopic;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $model common\models\Meeting */
/* @var $agendaTopics common\models\AgendaTopic */

//echo Yii::$app->params['separator'];
//echo Yii::$app->params['separatorClass'];

$this->title = 'รายละเอียดการประชุม';
$this->params['breadcrumbs'][] = [
    'label' => "รายการประชุมทั้งหมด " . Yii::$app->params['separator'] . " ",
    'url' => Url::to(['/meeting/default/index']),
    'class' => Yii::$app->params['separatorClass'],
    //'encode'=>true
];

$this->params['breadcrumbs'][] = [
    'label' => $this->title,
    #'url' => '',
    'class' => Yii::$app->params['separatorClass'],
];

$dr = Yii::$app->assetManager->getPublishedUrl('@t02/dist');
$user = Yii::$app->user->identity;
/*$this->params['toolbar'] = [
    [
        'type' => 'button',
        'label' => 'สร้างวาระ',
        'style' => 'success',
        'link' => 'javascript:;',
        'sub_button' => [
            [
                'type' => 'button',
                'label' => 'สร้างวาระ กวป', 'link' => Url::to(['/meeting/meeting/create', 'option' => '1',]),
                'text' => false,
                'icon' => 'flaticon2-add',
            ],
            [
                'type' => 'button',
                'label' => 'สร้างวาระ กบห', 'link' => Url::to(['/meeting/meeting/create', 'option' => '2']),
                'text' => false,
                'icon' => 'flaticon2-add',
            ],
        ],
    ],
];*/

\mootensai\components\JsBlock::widget(['viewFile' => '_scriptView', 'pos' => \yii\web\View::POS_END,
    'viewParams' => [

    ],
]);
?>
    <div class="card card-custom gutter-b example example-compact">
        <div class="bg-moph">
            <div class="example example-compact mt-5">
                <div class="example-tools justify-content-center mb-5">
                    <img class="img-circle max-h-150px" src="<?= $dr ?>/assets/media/logos/mukdahan-30.png">
                </div>
                <h4 class="example-tools justify-content-center">
                    <?= $this->title ?>
                </h4>
                <h4 class="example-tools justify-content-center">
                    <?= $model->meetingType->meeting_type_detail ?>
                </h4>
                <h4 class="example-tools justify-content-center">
                    <?= "ครั้งที่ " . $model->meeting_number . ' วันที่ ' . $model->formatMeetingDate() . ' เวลา ' . $model->setFormatMeetingTime() . " น." ?>
                </h4>
                <h4 class="example-tools justify-content-center">
                    <?= "ณ " . $model->meetingRoom->room_name . " " . Yii::$app->params['office'] ?>
                </h4>
            </div>
            <div class="example example-compact mt-5">
                <div class="row p-10">
                    <?php
                    if ($agendaTopics) {
                        /**
                         * @var $item AgendaTopic
                         */
                        foreach ($agendaTopics as $item) {

                            $style = "";
                            if ($item->level == 2) {
                                $style = "padding-left:30px;";
                            } else {
                                $style = "padding-left:5px;";
                            }

                            ?>
                            <div class="col-xl-12">
                                <!--begin::List Widget 19-->
                                <div class="card card-custom card-stretch gutter-b">
                                    <!--begin::Header-->
                                    <div class="card-header border-0 pt-6 mb-2">
                                        <h3 class="card-title align-items-start flex-column">
                                            <span class="card-label font-weight-bold font-size-h4 text-dark mb-3"><?= $item->topic->topic_name ?></span>
                                        </h3>
                                        <?php
                            if(!Yii::$app->user->isGuest) {
                                ?>
                                <div class="card-toolbar">
                                    <a href="#" class="btn btn-light-info btn-sm font-weight-bolder font-size-sm py-3 px-6 lovOpen"
                                       data-url="<?= Url::to(['/meeting/agenda/create', 'agenda_topic_id' => $item->agenda_topic_id, 'meeting_id' => $item->meeting_id, 'topic_id' => $item->topic_id]) ?>"><i
                                                class="flaticon2-add"></i>เพิ่ม</a>
                                </div>
                                <?php
                            }
                                ?>
                                    </div>
                                    <!--end::Header-->
                                    <!--begin::Body-->
                                    <div class="card-body pt-0">
                                        <?php
                                        Pjax::begin([
                                            'id' => 'wr-' . $item->agenda_topic_id,
                                        ]);
                                        ?>
                                        <?php
                                        if ($item->mtAgendas) {
                                            foreach ($item->mtAgendas as $agenda) {
                                                ?>
                                                <div class="d-flex align-items-center mb-5 bg-light-light rounded p-5">
                                                    <!--begin::Icon-->
                                                    <span class="svg-icon svg-icon-warning mr-5"></span>
                                                    <!--end::Icon-->
                                                    <!--begin::Title-->
                                                    <div class="d-flex flex-column flex-grow-1 mr-2">
                                                        <p class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"><?= $agenda->agenda_name ?>
                                                        </p>
                                                        <span class="text-muted font-weight-bold"><?= Yii::$app->formatter->asDatetime($agenda->updated_at, 'medium') ?></span>
                                                    </div>
                                                    <!--end::Title-->
                                                    <!--begin::Lable-->

                                                    <span class="font-weight-bolder text-warning py-1 font-size-lg"><?= $agenda->listDownloadFiles('agenda_files') ?></span>
                                                    <!--end::Lable-->
                                                <?php
                                                if(!Yii::$app->user->isGuest) {
                                                    ?>
                                                    <a href="#" class="btn btn-primary btn-icon btn-sm ml-2 lovOpen" title="เพิ่มวาระย่อย"
                                                       data-url="<?= Url::to(['/meeting/sub-agenda/create', 'id' => $agenda->agenda_id]) ?>">
                                                        <i class="flaticon2-plus"></i>
                                                    </a>
                                                    <a href="#" class="btn btn-warning btn-icon btn-sm ml-2 lovOpen" title="แก้ไขวาระ"
                                                       data-url="<?= Url::to(['/meeting/agenda/update', 'id' => $agenda->agenda_id]) ?>"> <i
                                                                class="flaticon2-edit"></i>
                                                    </a>
                                                    <a href="#" class="btn btn-danger btn-icon btn-sm ml-2 deleteAgenda" title="ลบวาระ"
                                                       data-url="<?= Url::to(['/meeting/agenda/delete', 'id' => $agenda->agenda_id]) ?>"> <i
                                                                class="flaticon2-delete"></i>
                                                    </a>
                                                    <?php
                                                }
                                                    ?>
                                                </div>

                                                <?php
                                                if($agenda->mtSubAgendas){
                                                    foreach ($agenda->mtSubAgendas as $mtSubAgenda) {
                                                        ?>

                                                        <div class="offset-sm-1 d-flex align-items-center mb-2 bg-light-warning rounded p-5">
                                                            <!--begin::Icon-->
                                                            <span class="svg-icon svg-icon-warning mr-5"></span>
                                                            <!--end::Icon-->
                                                            <!--begin::Title-->
                                                            <div class="d-flex flex-column flex-grow-1 mr-2">
                                                                <p class="font-weight-bold text-dark-75 text-hover-primary font-size-lg mb-1"><?= $mtSubAgenda->detail ?>
                                                                </p>
                                                                <span class="text-muted font-weight-bold"><?= Yii::$app->formatter->asDatetime($mtSubAgenda->updated_at, 'medium') ?></span>
                                                            </div>
                                                            <!--end::Title-->
                                                            <!--begin::Lable-->
                                                            <span class="font-weight-bolder text-warning py-1 font-size-lg"><?= $mtSubAgenda->listDownloadFiles('sub_files') ?></span>
                                                            <!--end::Lable-->
                                                        <?php
                                                        if(!Yii::$app->user->isGuest) {
                                                            ?>
                                                            <a href="#" class="btn btn-warning btn-icon btn-sm ml-2 lovOpen"
                                                               title="แก้ไขวาระ"
                                                               data-url="<?= Url::to(['/meeting/sub-agenda/update', 'id' => $mtSubAgenda->sub_agenda_id]) ?>">
                                                                <i class="flaticon2-edit"></i>
                                                            </a>
                                                            <a href="#" class="btn btn-danger btn-icon btn-sm ml-2 deleteAgenda"
                                                               title="ลบวาระ"
                                                               data-url="<?= Url::to(['/meeting/sub-agenda/delete', 'id' => $mtSubAgenda->sub_agenda_id]) ?>">
                                                                <i class="flaticon2-delete"></i>
                                                            </a>
                                                            <?php
                                                        }
                                                            ?>
                                                        </div>

                                                        <?php
                                                    }

                                                }
                                                ?>
                                                <?php
                                            }
                                        }
                                        Pjax::end();
                                        ?>
                                    </div>
                                    <!--end::Body-->
                                </div>
                                <!--end::List Widget 19-->
                            </div>
                            <?php
                        }
                    }
                    ?>

                </div>
            </div>
        </div>
        <div class="card-body">
            <?php
            if ($providerMtAgendaTopic->totalCount) {
                $gridColumnMtAgendaTopic = [
                    //['class' => 'yii\grid\SerialColumn'],
                    //'agenda_topic_id',

                    [
                        'class' => 'kartik\grid\ExpandRowColumn',
                        'label' => 'ระเบียบวาระการประชุม',
                        'width' => '5%',

                        'value' => function ($model, $key, $index, $column) {
                            return GridView::ROW_EXPANDED;
                        },
                        'detail' => function ($model, $key, $index, $column) {
                            return Yii::$app->controller->renderPartial('//../modules/views/meeting/_dataMtAgenda', ['model' => $model]);
                        },
                        //'headerOptions' => ['class' => 'kartik-sheet-style'],
                        'expandOneOnly' => false,
                        'expandIcon' => '<span class="flaticon2-next"></span>',
                        'collapseIcon' => '<span class="flaticon2-down"></span>',
                        'defaultHeaderState' => GridView::ROW_EXPANDED,
                    ],

                    [
                        'attribute' => 'topic_id',
                        'label' => 'ระเบียบวาระการประชุม',
                        'width' => '80%',
                        'contentOptions' => function ($model) {
                            if ($model->level == 2) {
                                return [
                                    'style' => 'padding-left:50px;',
                                ];
                            } else {
                                return [
                                    'style' => 'padding-left:10px;',
                                ];
                            }
                        },
                        'value' => function ($model) {
                            $wara = $model->topic->topic_name;
                            $table = Yii::$app->controller->renderPartial('//../modules/views/meeting/_dataMtAgenda', ['model' => $model]);
                            return $wara . $table;
                        },
                        'format' => 'raw',
                    ],

                    [
                        'class' => 'kartik\grid\ActionColumn',
                        'template' => '<div  class="btn-group btn-group-sm text-center" role="group">{create}</div>', // approve
                        'header' => 'จัดการ',
                        'vAlign' => GridView::ALIGN_TOP,
                        'buttonOptions' => ['class' => 'btn btn-default'],
                        'viewOptions' => [
                            'class' => 'btn btn-primary',
                            //'data-toggle' => 'tooltip',
                        ],
                        'updateOptions' => [
                            'class' => 'updateUser btn btn-warning',
                            //'data-toggle' => 'tooltip',
                            'title' => 'แก้ไข',
                        ],
                        'deleteOptions' => [
                            'class' => 'btn btn-danger deleteCommandItem',
                            //'data-toggle' => 'tooltip',
                            //'data-confirm' => 'คุณต้องการลบข้อมูลอุปกรณ์นี้ ?',
                            //'data-pjax'=>1
                        ],
                        'noWrap' => true,
                        'contentOptions' => [
                            'noWrap' => true,
                            // 'style' => 'width:160px;',
                        ],
                        'buttons' => [

                            'create' => function ($url, $model) use ($user) {
                                if ($user->level == 3) {
                                    return Html::a('<span class="fa fa-plus"></span>', '#', [
                                        'title' => 'มอบภารกิจ/เพิ่มรายละเอียด',
                                        'class' => 'btn btn-primary lovOpen',
                                        'data-url' => Url::to(['meeting-command-items/create', 'id' => $model->agenda_topic_id]),
                                    ]);
                                } else {
                                    return '';
                                }
                            },
                        ],
                    ]
                    //'order',
                ];
                Gridview::widget([
                    'dataProvider' => $providerMtAgendaTopic,
                    'pjax' => true,
                    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-mt-agenda-topic']],
                    'panel' => false /*[
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => false,
                    'before'=> false,
                ]*/,
                    'columns' => $gridColumnMtAgendaTopic,
                    'bordered' => true,
//                'showHeader' => false,
//                'hover' => true,
                    'tableOptions' => [
                        //'class'=>'bg-success'
                    ],
                    'headerRowOptions' => [
                        //'class' => 'thead-light',
                    ],
                ]);
            }
            ?>

            <!--<table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th colspan="3">ระเบียบวาระการประชุม</th>
            </tr>
            </thead>
            <tbody>
            <?php
            /*            if ($agendaTopics) {
                            foreach ($agendaTopics as $item) {

                                $style = "";
                                if ($item->level == 2) {
                                    $style = "padding-left:30px;";
                                } else {
                                    $style = "padding-left:5px;";
                                }
                                */ ?>
                    <tr>
                        <td width="90%" colspan="2" style="<? /*= $style */ ?>"><? /*= $item->topic->topic_name */ ?>
                        </td>
                        <td class="align-items-center" style="align-items: center;"><a href="#" class="btn btn-sm btn-primary lovOpen" data-url="<? /*=Url::to(['/meeting/agenda/create','agenda_topic_id'=>$item->agenda_topic_id,'meeting_id'=>$item->meeting_id,'topic_id'=>$item->topic_id])*/ ?>"><i class="flaticon2-add"></i> เพิ่ม
                            </a>
                        </td>
                    </tr>

                    <?php
            /*                }
                        }
                        */ ?>
            </tbody>
        </table>-->


        </div>
    </div>

<?php
$this->registerJsFile($dr . "/assets/js/agenda.js",
    [
        'position' => View::POS_END,
        'depends' => '\t02\assets\MainAsset',
    ]);

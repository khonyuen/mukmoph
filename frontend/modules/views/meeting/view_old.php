<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Meeting */

$this->title = $model->meeting_id;
$this->params['breadcrumbs'][] = ['label' => 'Meeting', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meeting-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Meeting'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
<?=             
             Html::a('<i class="fa glyphicon glyphicon-hand-up"></i> ' . 'PDF', 
                ['pdf', 'id' => $model->meeting_id],
                [
                    'class' => 'btn btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => 'Will open the generated PDF file in a new window'
                ]
            )?>
            
            <?= Html::a('Update', ['update', 'id' => $model->meeting_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->meeting_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'meeting_id',
        [
            'attribute' => 'meetingType.meeting_type_id',
            'label' => 'Meeting Type',
        ],
        'meeting_number',
        'meeting_time',
        'meeting_date',
        'meeting_status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerMtAgenda->totalCount){
    $gridColumnMtAgenda = [
        ['class' => 'yii\grid\SerialColumn'],
            'agenda_id',
                        'agenda_name:ntext',
            'agenda_files:ntext',
            'agenda_files_tmp',
            [
                'attribute' => 'agendaTopicType.topic_id',
                'label' => 'Agenda Topic Type'
            ],
            'agenda_status',
            'agenda_order',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMtAgenda,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-mt-agenda']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Mt Agenda'),
        ],
        'columns' => $gridColumnMtAgenda
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerMtAgendaTopic->totalCount){
    $gridColumnMtAgendaTopic = [
        ['class' => 'yii\grid\SerialColumn'],
            'agenda_topic_id',
            [
                'attribute' => 'topic.topic_id',
                'label' => 'Topic'
            ],
                        'order',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMtAgendaTopic,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-mt-agenda-topic']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Mt Agenda Topic'),
        ],
        'columns' => $gridColumnMtAgendaTopic
    ]);
}
?>

    </div>
    <div class="row">
        <h4>MtMeetingType<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnMtMeetingType = [
        'meeting_type_id',
        'meeting_type_name',
        'meeting_type_status',
    ];
    echo DetailView::widget([
        'model' => $model->meetingType,
        'attributes' => $gridColumnMtMeetingType    ]);
    ?>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Meeting */

$this->title = $model->meeting_id;
$this->params['breadcrumbs'][] = ['label' => 'Meeting', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="meeting-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Meeting'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'meeting_id',
        [
                'attribute' => 'meetingType.meeting_type_id',
                'label' => 'Meeting Type'
            ],
        'meeting_number',
        'meeting_time',
        'meeting_date',
        'meeting_status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerMtAgenda->totalCount){
    $gridColumnMtAgenda = [
        ['class' => 'yii\grid\SerialColumn'],
        'agenda_id',
                'agenda_name:ntext',
        'agenda_files:ntext',
        'agenda_files_tmp',
        [
                'attribute' => 'agendaTopicType.topic_id',
                'label' => 'Agenda Topic Type'
            ],
        'agenda_status',
        'agenda_order',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMtAgenda,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Mt Agenda'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnMtAgenda
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerMtAgendaTopic->totalCount){
    $gridColumnMtAgendaTopic = [
        ['class' => 'yii\grid\SerialColumn'],
        'agenda_topic_id',
        [
                'attribute' => 'topic.topic_id',
                'label' => 'Topic'
            ],
                'order',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMtAgendaTopic,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Mt Agenda Topic'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnMtAgendaTopic
    ]);
}
?>
    </div>
</div>

<?php
use yii\helpers\Url;

?>
<script>
    /*$(".addWrBtn").on("click",function (e) {
        e.preventDefault();
        let url = $(this).data('url');

    })*/

    $('body').on('click', '.lovOpen', function (e) {
        e.preventDefault();
        $('#modal_main').find('#modalContent').html('');
        $('#modal_main').modal('show').find('#modalContent')
            .load($(this).attr('data-url'));
        //dynamiclly set the header for the modal
        //document.getElementById('modalHeader').innerHTML = '<h4>' + $(this).attr('data-text') + '</h4>';

        if ($(this).attr('data-title')) {
            $("#modalHeader").html("<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button><h4>" + $(this).attr('data-title') + "</h4>");
        } else {
            $("#modalHeader").remove();
        }
    });

    $('.modal').on('hidden.bs.modal', function (e) {
        $('body').addClass('modal-open');
        console.log('close');
    });
</script>

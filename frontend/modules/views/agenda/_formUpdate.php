<?php

use common\models\AgendaTopic;
use common\models\TopicType;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Agenda */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $modelTT TopicType */
/* @var $modelAT AgendaTopic */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos' => \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'MtSubAgenda',
        'relID' => 'mt-sub-agenda',
        'value' => \yii\helpers\Json::encode($model->mtSubAgendas),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0,
    ],
]);

$this->title = "บันทึกวาระ "."(".$modelTT->topic_name.")";
?>

<div class="agenda-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'class' => 'form',
            'enctype' => 'multipart/form-data',
        ],
        'id' => 'update-agenda',
        'validateOnSubmit' => true,
        'validateOnBlur' => false,
        'validateOnChange' => false,
        'enableAjaxValidation' => true,
        'validationUrl' => Url::to(['validate-agenda']),
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'label' => 'col-sm-2',
                'offset' => 'offset-sm-0',
                'wrapper' => 'col-sm-10',
                'error' => '',
                'hint' => '',
            ],
        ],
        'layout' => 'horizontal',
    ]);

    echo Html::activeHiddenInput($model,'agenda_topic_id');
    echo Html::activeHiddenInput($model,'meeting_id');
    echo Html::activeHiddenInput($model,'agenda_topic_type');
    echo Html::activeHiddenInput($model,'agenda_files_tmp');
    ?>

    <div class="card card-custom gutter-b example example-compact">
        <div class="card-header">
            <h3 class="card-title"><?= $this->title ?></h3>
        </div>
        <div class="card-body">
            <?= $form->errorSummary($model); ?>

            <?= $form->field($model, 'agenda_name')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'agenda_files[]')->widget(\kartik\widgets\FileInput::className(), [
                'language' => 'th',
                'options' => [
                    'multiple' => true,
                ],
                'pluginOptions' => [
                    'showPreview' => false,
                    'showRemove' => true,
                    'showUpload' => false,
                    'overwriteInitial' => true,
                    'autoReplace' => true,
                    //'uploadUrl' => Url::to(['meeting-responsible/create']),
                    'initialPreview' => $model->initialPreview($model->agenda_files, 'agenda_files', 'files'),
                    'previewFileType' => ['pdf', 'html', 'text', 'video', 'audio', 'flash', 'object','rar','zip'],
                    'purifyHtml' => true,
                    //'deleteUrl'=>Url::to(['area/deletefile','id'=>$model->area_id, 'field'=>'area_pic','fileName'=>'']),
                    'initialPreviewAsData' => true,
                    'initialPreviewConfig' => $model->initialPreview($model->agenda_files, 'agenda_files', 'config'),
                ],
                //''
            ]) ?>

            <?= $form->field($model, 'agenda_status')->inline()->radioList([
                    '0'=>'ปิด',
                    '1'=>'เปิด',
            ]) ?>
            <div class="row">
                <div class="col-12">
                    <?= $form->field($model, 'agenda_order')->textInput(['placeholder' => '','type'=>'number','width'=>'250px;']) ?>
                </div>
            </div>




        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-2"></div>
                <div class="col-10">
                    <?= Html::submitButton($model->isNewRecord ? 'บันทึก' : 'แก้ไข', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    <?= Html::a('ยกเลิก', '#', ['class' => 'btn btn-danger lovClose','data-dismiss'=>'modal']) ?></div>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Agenda */

$this->title = $model->agenda_id;
$this->params['breadcrumbs'][] = ['label' => 'Agenda', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agenda-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Agenda'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->agenda_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->agenda_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'agenda_id',
        [
            'attribute' => 'meeting.meeting_id',
            'label' => 'Meeting',
        ],
        'agenda_name:ntext',
        'agenda_files:ntext',
        'agenda_files_tmp',
        [
            'attribute' => 'agendaTopicType.topic_id',
            'label' => 'Agenda Topic Type',
        ],
        [
            'attribute' => 'agendaTopic.agenda_topic_id',
            'label' => 'Agenda Topic',
        ],
        'agenda_status',
        'agenda_order',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>MtMeeting<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnMtMeeting = [
        'meeting_type',
        'meeting_month',
        'meeting_year',
        'meeting_room',
        'meeting_number',
        'meeting_time',
        'meeting_date',
        'meeting_status',
    ];
    echo DetailView::widget([
        'model' => $model->meeting,
        'attributes' => $gridColumnMtMeeting    ]);
    ?>
    <div class="row">
        <h4>MtTopicType<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnMtTopicType = [
        'topic_id',
        'topic_name',
        'topic_order',
        'topic_level',
    ];
    echo DetailView::widget([
        'model' => $model->agendaTopicType,
        'attributes' => $gridColumnMtTopicType    ]);
    ?>
    <div class="row">
        <h4>MtAgendaTopic<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnMtAgendaTopic = [
        'topic_id',
        [
            'attribute' => 'meeting.meeting_id',
            'label' => 'Meeting',
        ],
        'order',
        'level',
    ];
    echo DetailView::widget([
        'model' => $model->agendaTopic,
        'attributes' => $gridColumnMtAgendaTopic    ]);
    ?>
    
    <div class="row">
<?php
if($providerMtSubAgenda->totalCount){
    $gridColumnMtSubAgenda = [
        ['class' => 'yii\grid\SerialColumn'],
            'sub_agenda_id',
                        'agenda_topic_id',
            'detail:ntext',
            'sub_order',
            'sub_files:ntext',
            'sub_files_tmp',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMtSubAgenda,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-mt-sub-agenda']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Mt Sub Agenda'),
        ],
        'export' => false,
        'columns' => $gridColumnMtSubAgenda
    ]);
}
?>

    </div>
</div>

<div class="form-group" id="add-mt-sub-agenda">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'MtSubAgenda',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'sub_agenda_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'agenda_topic_id' => ['type' => TabularForm::INPUT_TEXT],
        'detail' => ['type' => TabularForm::INPUT_TEXTAREA],
        'sub_order' => ['type' => TabularForm::INPUT_TEXT],
        'sub_files' => ['type' => TabularForm::INPUT_TEXTAREA],
        'sub_files_tmp' => ['type' => TabularForm::INPUT_TEXT],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowMtSubAgenda(' . $key . '); return false;', 'id' => 'mt-sub-agenda-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Mt Sub Agenda', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowMtSubAgenda()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>


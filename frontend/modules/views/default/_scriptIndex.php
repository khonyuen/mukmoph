<?php
/**
 * @var $value array
 * @var $type array
 */

//print_r($value);
?>

<script>
    <?php
    //    foreach ($value as $year) {
    //
    //    }
    ?>

    $(".filterMeetingYear").on("click", function (e) {
        e.preventDefault();
        let url = window.location.href;
        let year = $(this).data('year');
        let type = $(this).data('type');
        let container = $(this).data('container');
        console.log(container, url, year, type);
        // kv-pjax-container-meeting-
        let fullUrl = "";
        if (url.indexOf('?') != -1) {
            fullUrl = url + "&MeetingSearch[meeting_year]=" + year + "&MeetingSearch[meeting_type]=" + type;
        } else {
            fullUrl = url + "?MeetingSearch[meeting_year]=" + year + "&MeetingSearch[meeting_type]=" + type;
        }

        $.pjax.reload("#" + container, {
            url: fullUrl
        });
    })

    var crud = function () {
        var deleteItem = function () {

            $("body").on("click", '.deleteItem', function (e) {
                e.preventDefault();
                var url = $(this).attr('href');
                let container = $(this).data('container');
                swal.fire({
                    title: 'ยืนยันการลบข้อมูล ?',
                    text: "คุณต้องการลบข้อมูลนี้ !",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'ใช่, ลบมันซะ!'
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: url,
                            type: "post",
                            //data: {key: id},
                            success: function (response) {
                                var res = JSON.parse(response);
                                console.log(res.msg);

                                if(res.result) {
                                    swal.fire(
                                        'ลบข้อมูลเรียบร้อย!',
                                        'success');

                                    $.pjax.reload({container: "#"+container});
                                }else {
                                    swal.fire(
                                        'ไม่สามารถลบได้! '+res.msg,
                                        'warning');
                                }
                            },
                            error: function (error) {
                                console.log(error)
                            }
                        });
                    }
                });
            });
        };

        return {
            // public functions
            init: function () {
                deleteItem();
            },
        };
    }();

    window.addEventListener('load', function () {
        crud.init();
    });
</script>

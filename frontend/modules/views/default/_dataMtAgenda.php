<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->mtAgendas,
        'key' => 'agenda_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        'agenda_id',
        'agenda_name:ntext',
        'agenda_files:ntext',
        'agenda_files_tmp',
        [
                'attribute' => 'agendaTopicType.topic_id',
                'label' => 'Agenda Topic Type'
            ],
        'agenda_status',
        'agenda_order',
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'mt-agenda'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);

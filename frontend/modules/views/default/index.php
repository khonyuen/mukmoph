<?php
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\MeetingSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use common\models\MeetingType;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'วาระการประชุม';
$this->params['breadcrumbs'][] = [
    'label' => $this->title,
    'url' => '#',
    'class' => Yii::$app->params['separatorClass'],
];
$this->params['toolbar'] = [
    [
        'type' => 'button',
        'label' => 'สร้างวาระ',
        'style' => 'success',
        'link' => 'javascript:;',
        'sub_button' => [
            [
                'type' => 'button',
                'label' => 'สร้างวาระ กวป', 'link' => Url::to(['/meeting/meeting/create', 'option' => '1',]),
                'text' => false,
                'icon' => 'flaticon2-add',
            ],
            [
                'type' => 'button',
                'label' => 'สร้างวาระ กบห', 'link' => Url::to(['/meeting/meeting/create', 'option' => '2']),
                'text' => false,
                'icon' => 'flaticon2-add',
            ],
        ],
    ],
];

$meetingType = MeetingType::getMeetingType(false);
$user = Yii::$app->user->identity;
?>
<div class="row">
    <div class="col-12">
        <div class="card card-custom gutter-b">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label"><?= $this->title ?></h3>
                </div>
                <div class="card-toolbar">
                    <ul class="nav nav-light-success nav-bold nav-pills">
                        <?php
                        $active = true;
                        foreach ($meetingType as $item) {
                            if ($active)
                                $tActive = "active";
                            ?>

                            <li class="nav-item">
                                <a class="nav-link <?= $tActive ?>" data-toggle="tab" href="#meeting_<?= $item->meeting_type_id ?>">
                                    <span class="nav-icon"><i class="flaticon2-calendar-1"></i></span>
                                    <span class="nav-text"><?= $item->meeting_type_name ?></span>
                                </a>
                            </li>

                            <?php
                            $active = false;
                            $tActive = "";
                        }
                        ?>
                    </ul>
                </div>
            </div>

            <div class="card-body">
                <div class="tab-content">
                    <?php
                    $active = true;

                    foreach ($meetingType as $item) {
                        if ($active)
                            $tActive = "active";

                        //$searchModel->meeting_type = $item->meeting_type_id;



                        $dataProvider = $searchModel->search([
                            'MeetingSearch' => [
                                'meeting_type' => $item->meeting_type_id,
                                'meeting_year' => (date('Y')+543),
                            ],
                        ]);


                        ?>
                        <div class="tab-pane fade show <?= $tActive ?>" id="meeting_<?= $item->meeting_type_id ?>" role="tabpanel"
                             aria-labelledby="meeting_<?= $item->meeting_type_id ?>">

                            <?php
                            // grid column
                            $gridColumn = [
                                //['class' => 'yii\grid\SerialColumn'],
                                /*[
                                    'class' => 'kartik\grid\ExpandRowColumn',
                                    'width' => '50px',
                                    'vAlign' => 'top',
                                    'value' => function ($model, $key, $index, $column) {
                                        return GridView::ROW_COLLAPSED;
                                    },
                                    'detail' => function ($model, $key, $index, $column) {
                                        return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
                                    },
                                    'headerOptions' => ['class' => 'kartik-sheet-style'],
                                    'expandOneOnly' => true,
                                ],*/
                                //'meeting_id',
//                                [
//                                    'attribute' => 'meeting_type',
//                                    //'label' => 'Meeting Type',
//                                    'value' => function ($model) {
//                                        return $model->meetingType->meeting_type_name;
//                                    },
//                                    'filterType' => GridView::FILTER_SELECT2,
//                                    'filter' => \yii\helpers\ArrayHelper::map(\common\models\MeetingType::find()->asArray()->all(), 'meeting_type_id', 'meeting_type_id'),
//                                    'filterWidgetOptions' => [
//                                        'pluginOptions' => ['allowClear' => true],
//                                    ],
//                                    'filterInputOptions' => ['placeholder' => 'Mt meeting type', 'id' => 'grid-meeting-search-meeting_type'],
//                                ],

                                [
                                    'attribute' => 'meeting_month',
                                    'value' => function ($model) {
                                        return $model->meetingThaiMonth();
                                    },
                                ],
                                'meeting_number',
                                [
                                    'attribute' => 'meeting_date',
                                    'value' => function ($model) {
                                        return $model->formatMeetingDate();
                                    },
                                ],
                                [
                                    'attribute' => 'meeting_time',
                                    'value' => function ($model) {
                                        return $model->meeting_time;
                                    },
                                    'format'=>['time','php:H:i']
                                ],
                                [
                                    'class' => 'kartik\grid\ActionColumn',
                                    'template' => '{view} {update} {delete}',
                                    'header' => 'จัดการ',
                                    'vAlign' => 'top',
                                    'headerOptions' => [
                                        'style' => 'align:center',
                                    ],
                                    'noWrap' => false,
                                    'options' => [
                                        'width' => '180px',
                                    ],
                                    'buttonOptions' => [
                                        'class' => 'btn btn-default btn-sm',
                                    ],
                                    'buttons' => [
                                        'view' => function ($url, $model) {
                                            return Html::a('<i class="flaticon2-search"></i>', Url::to(['/meeting/meeting/view', 'id' => $model->meeting_id]), [
                                                'title' => 'ดูข้อมูล',
                                                'class' => 'btn btn-primary btn-icon',
                                                'data-pjax' => 0,
                                            ]);
                                        }, 'update' => function ($url, $model) use ($user) {
                                            if (3 == $user->level) {
                                                return Html::a('<i class="flaticon2-edit"></i>', Url::to(['/meeting/meeting/update', 'id' => $model->meeting_id]), [
                                                    'title' => 'แก้ไขข้อมูล',
                                                    'class' => 'btn btn-warning btn-icon',
                                                    'data-pjax' => 0,
                                                ]);
                                            } else {
                                                return '';
                                            }
                                        }, 'delete' => function ($url, $model) use ($user) {
                                            if (3 == $user->level) {
                                                return Html::a('<i class="flaticon2-delete"></i>', Url::to(['/meeting/meeting/delete', 'id' => $model->meeting_id]), [
                                                    'title' => 'ลบข้อมูล',
                                                    'class' => 'btn btn-danger btn-icon deleteItem',
                                                ]);
                                            } else {
                                                return '';
                                            }
                                        },
                                    ],
                                ],
                            ];
                            ?>

                            <div class="row gutter-b">
                                <div class="col-12">
                                    <span class="h3">ปี พ.ศ.  </span>

                                </div>
                            </div>


                            <?= GridView::widget([
                                'dataProvider' => $dataProvider,
                                //'filterModel' => $searchModel,
                                'columns' => $gridColumn,
                                'pjax' => true,
                                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-meeting']],
                                'panel' => false,
                                'layout' => "{items}\n{summary}\n{pager}",
                                /*[
                                    'type' => GridView::TYPE_PRIMARY,
                                    'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
                                ],*/
                                // your toolbar can include the additional full export menu
//                                'toolbar' => [
//                                    '{export}',
//                                    ExportMenu::widget([
//                                        'dataProvider' => $dataProvider,
//                                        'columns' => $gridColumn,
//                                        'target' => ExportMenu::TARGET_BLANK,
//                                        'fontAwesome' => true,
//                                        'dropdownOptions' => [
//                                            'label' => 'Full',
//                                            'class' => 'btn btn-default',
//                                            'itemsBefore' => [
//                                                '<li class="dropdown-header">Export All Data</li>',
//                                            ],
//                                        ],
//                                    ]),
//                                ],
                            ]); ?>

                        </div>
                        <?php
                        $active = false;
                        $tActive = "";
                    }
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>


<?php

namespace frontend\modules\controllers;

use common\models\Meeting;
use common\models\MeetingType;
use frontend\models\MeetingSearch;
use Yii;
use yii\web\Controller;

/**
 * Default controller for the `meeting` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        //
        //$currentYear = (date('Y')+543);
        // count meeting type
        $meetingType = MeetingType::getMeetingType(false);
        // count year
        $meetingYearCate = Meeting::find()->select(['meeting_year'])->groupBy('meeting_year')->column();

        $dataPs = [];
        if ($meetingType != []) {
            $params = Yii::$app->request->queryParams;

            //print_r($params);

            foreach ($meetingType as $type) {
                $searchModel = new MeetingSearch();

                $params['MeetingSearch']['meeting_type'] = $type->meeting_type_id;

                $dataProvider = $searchModel->search($params);
                $dataPs[$type->meeting_type_id] = $dataProvider;
            }
        }


//        $searchModel = new MeetingSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //print_r(Yii::$app->request->queryParams);
        return $this->render('index_new', [
            'meetingType'=>$meetingType,
            'meetingYearCate'=>$meetingYearCate,
            'searchModel' => $searchModel,
            'dataPs' => $dataPs,
        ]);
    }
}

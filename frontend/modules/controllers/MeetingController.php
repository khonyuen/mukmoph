<?php

namespace frontend\modules\controllers;

use common\models\AgendaTopic;
use common\models\Meeting;
use frontend\models\MeetingSearch;
use Yii;
use yii\bootstrap4\ActiveForm;
use yii\db\Exception;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * MeetingController implements the CRUD actions for Meeting model.
 */
class MeetingController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index', 'view', 'create', 'update',
                            'delete', 'pdf', 'add-mt-agenda',
                            'add-mt-agenda-topic', 'validate-meeting',
                            'delete-agenda-topic',
                            'add-agenda-topic'
                        ],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'view',
                        ],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => false,
                    ],
                ],
            ],
        ];
    }

    public function actionValidateMeeting()
    {
        $data = Yii::$app->request->post('Meeting');
        if (null != $data['meeting_id']) {
            $model = Meeting::findOne($data['meeting_id']);
        } else {
            //ถ้าเป็นการบันทึกใหม่ ให้สร้าง โมเดลใหม่
            $model = new Meeting();
            //$budget = [new ProjectDetailBudget()];
            //$model->scenario = 'insert';
        }
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            $model->_agenda_topic = Yii::$app->request->post('Meeting')['_agenda_topic'];
            //print_r($model->_meeting_command);
            /*$data = Yii::$app->request->post('ProjectDetailBudget',[]);
            foreach (array_keys($data) as $index) {
                $budget[$index] = new ProjectDetailBudget();
            }
            Model::loadMultiple($budget,Yii::$app->request->post());*/

            Yii::$app->response->format = Response::FORMAT_JSON;
            //return ActiveForm::validateMultiple($budget);
            return ActiveForm::validate($model);
        }
    }

    /**
     * Lists all Meeting models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MeetingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Meeting model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $agendaTopic = AgendaTopic::find()->joinWith(['topic'])->where(['meeting_id'=>$id])->orderBy('order asc'); //->indexBy('agenda_topic_id');
        /*$providerMtAgenda = new \yii\data\ArrayDataProvider([
            'allModels' => $model->mtAgendas,
        ]);*/
        $providerMtAgendaTopic = new \yii\data\ActiveDataProvider([
            'query' => $agendaTopic,
            'sort' => [
                'attributes'=>[
                    'order'=>['default'=>SORT_ASC]
                ]
            ]
        ]);

        //$agenda = $agendaTopic->mtAgendas;


        return $this->render('view', [
            'model' => $this->findModel($id),
            //'providerMtAgenda' => $providerMtAgenda,
            'providerMtAgendaTopic' => $providerMtAgendaTopic,
            'agendaTopics'=>$agendaTopic->all()
        ]);
    }

    /**
     * Finds the Meeting model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Meeting the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Meeting::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates a new Meeting model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($option)
    {
        $model = new Meeting();

        if ($model->load(Yii::$app->request->post())) {
            try {
                $ts = Yii::$app->db->beginTransaction();
                $model->setFormatMeetingDateDb();
                if ($model->save()) {
                    $agenda_topic = AgendaTopic::createModel($model->primaryKey, $model->_agenda_topic);
                    $result = [
                        'title' => 'ผลการบันทึก',
                        'message' => 'บันทึกข้อมูลการประชุมสำเร็จ',
                        'status' => 'success',
                        'url' => Url::to(['/meeting/meeting/view', 'id' => $model->meeting_id]),
                    ];
                    $ts->commit();
                    //Yii::$app->response->format = Response::FORMAT_JSON;
                    return Json::encode($result);
                } else {
                    throw new HttpException(implode(',', $model->getErrorSummary(true)));
                }
            } catch (\Exception $exception) {
                $ts->rollBack();
                $result = [
                    'title' => 'พบข้อผิดพลาด',
                    'message' => '' . $exception->getMessage(),
                    'status' => 'error',
                ];
                //Yii::$app->response->format = Response::FORMAT_JSON;
                return Json::encode($result);
            }
            //return $this->redirect(['view', 'id' => $model->meeting_id]);
        } else {
            $model->meeting_year = (date('Y') + 543);
            $model->meeting_status = 1;
            $model->meeting_type = $option;
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Meeting model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            try {
                $ts = Yii::$app->db->beginTransaction();
                $model->setFormatMeetingDateDb();
                if ($model->save()) {
                    $result = [
                        'title' => 'ผลการบันทึก',
                        'message' => 'แก้ไขข้อมูลการประชุมสำเร็จ',
                        'status' => 'success',
                        'url' => Url::to(['/meeting/meeting/view', 'id' => $model->meeting_id]),
                    ];
                    $ts->commit();
                    //Yii::$app->response->format = Response::FORMAT_JSON;
                    return Json::encode($result);
                } else {
                    throw new HttpException(implode(',', $model->getErrorSummary(true)));
                }
            } catch (\Exception $exception) {
                $ts->rollBack();
                $result = [
                    'title' => 'พบข้อผิดพลาด',
                    'message' => '' . $exception->getMessage(),
                    'status' => 'error',
                ];
                //Yii::$app->response->format = Response::FORMAT_JSON;
                return Json::encode($result);
            }
        } else {
            $topicAgenda = $model->mtAgendaTopics;

            $a = ArrayHelper::map($topicAgenda, 'agenda_topic_id', function ($model) {
                return $model['topic_id'] . '|' . $model['meeting_id']. '|' . $model['order']. '|' . $model['level'];
            });
            //print_r($a);

            $model->_agenda_topic = $a;
            $model->setFormatMeetingDatePicker();
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Meeting model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $ts = Yii::$app->db->beginTransaction();
        try {
            if ($id) {
                $meeting = Meeting::findOne($id);

                if ($meeting::deleteAll(['=', 'meeting_id', $id])) {
                    $ts->commit();
                }
            } else {
                $ids = Yii::$app->request->post('key');
                if (!is_null($ids)) {
                    $ids = explode(',', $ids);
                    $model = Meeting::deleteAll(['in', 'meeting_id', $ids]);
                    $ts->commit();
                }
            }
            return Json::encode([
                'result' => true,
                'msg' => 'ลบข้อมูลเรียบร้อย',
                'options' => [],
            ]);
        } catch (Exception $exception) {
            $ts->rollBack();
            return Json::encode([
                'result' => false,
                'msg' => 'ไม่สามารถลบข้อมูลได้ เนื่องจาก ' . $exception->getMessage(),
                'options' => [],
            ]);
        }
    }

    public function actionDeleteAgendaTopic($value)
    {
        $ts = Yii::$app->db->beginTransaction();
        try {
            if ($value) {
                $ex = explode('|',$value);
                $agendaTp = AgendaTopic::findOne(['topic_id'=>$ex[0],'meeting_id'=>$ex[1]]);

                if ($agendaTp!=null && $agendaTp->delete()) {
                    $ts->commit();
                    return Json::encode([
                        'result' => true,
                        'msg' => 'ลบข้อมูลเรียบร้อย',
                        'options' => [],
                    ]);
                }else{
                    throw new Exception(implode(',',$agendaTp->getErrorSummary(true)));
                }
            }

        } catch (Exception $exception) {
            $ts->rollBack();
            return Json::encode([
                'result' => false,
                'msg' => 'ไม่สามารถลบข้อมูลได้ เนื่องจาก ' . $exception->getMessage(),
                'options' => [],
            ]);
        }
    }

    public function actionAddAgendaTopic($value)
    {
        $ts = Yii::$app->db->beginTransaction();
        try {
            if ($value) {
                $ex = explode('|',$value);
                $agendaTp = new AgendaTopic();

                $agendaTp->topic_id = $ex[0];
                $agendaTp->meeting_id = $ex[1];
                $agendaTp->order = $ex[2];
                $agendaTp->level = $ex[3];

                if ($agendaTp->save()) {
                    $ts->commit();
                    return Json::encode([
                        'result' => true,
                        'msg' => 'เพิ่มข้อมูลเรียบร้อย',
                        'options' => [],
                    ]);
                }else{
                    throw new Exception(implode(',',$agendaTp->getErrorSummary(true)));
                }
            }

        } catch (Exception $exception) {
            $ts->rollBack();
            return Json::encode([
                'result' => false,
                'msg' => 'ไม่สามารถบันทึกข้อมูลได้ เนื่องจาก ' . $exception->getMessage(),
                'options' => [],
            ]);
        }
    }

    /**
     *
     * Export Meeting information into PDF format.
     * @param integer $id
     * @return mixed
     */
    public function actionPdf($id)
    {
        $model = $this->findModel($id);
        $providerMtAgenda = new \yii\data\ArrayDataProvider([
            'allModels' => $model->mtAgendas,
        ]);
        $providerMtAgendaTopic = new \yii\data\ArrayDataProvider([
            'allModels' => $model->mtAgendaTopics,
        ]);

        $content = $this->renderAjax('_pdf', [
            'model' => $model,
            'providerMtAgenda' => $providerMtAgenda,
            'providerMtAgendaTopic' => $providerMtAgendaTopic,
        ]);

        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_CORE,
            'format' => \kartik\mpdf\Pdf::FORMAT_A4,
            'orientation' => \kartik\mpdf\Pdf::ORIENT_PORTRAIT,
            'destination' => \kartik\mpdf\Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:18px}',
            'options' => ['title' => \Yii::$app->name],
            'methods' => [
                'SetHeader' => [\Yii::$app->name],
                'SetFooter' => ['{PAGENO}'],
            ],
        ]);

        return $pdf->render();
    }

    /**
     * Action to load a tabular form grid
     * for MtAgenda
     * @return mixed
     * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
     *
     * @author Yohanes Candrajaya <moo.tensai@gmail.com>
     */
    public function actionAddMtAgenda()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('MtAgenda');
            if (!empty($row)) {
                $row = array_values($row);
            }
            if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formMtAgenda', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Action to load a tabular form grid
     * for MtAgendaTopic
     * @return mixed
     * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
     *
     * @author Yohanes Candrajaya <moo.tensai@gmail.com>
     */
    public function actionAddMtAgendaTopic()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('MtAgendaTopic');
            if (!empty($row)) {
                $row = array_values($row);
            }
            if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formMtAgendaTopic', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

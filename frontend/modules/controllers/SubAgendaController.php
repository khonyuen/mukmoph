<?php

namespace frontend\modules\controllers;

use common\models\Agenda;
use Yii;
use common\models\SubAgenda;
use yii\bootstrap4\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseFileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * SubAgendaController implements the CRUD actions for SubAgenda model.
 */
class SubAgendaController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'create',
                            'delete',
                            'deletefile',
                            'download',
                            'download-zip',
                            'index',
                            'update',
                            'view',
                            'validate-sub-agenda'
                        ],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'view','download','download-zip'
                        ],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => false,
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all SubAgenda models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => SubAgenda::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SubAgenda model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $providerMtSubAgendaFile = new \yii\data\ArrayDataProvider([
            'allModels' => $model->mtSubAgendaFiles,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerMtSubAgendaFile' => $providerMtSubAgendaFile,
        ]);
    }

    /**
     * Creates a new SubAgenda model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new SubAgenda();

        if ($model->load(Yii::$app->request->post())) {
            $agenda = Agenda::findOne($id);
            $ts = Yii::$app->db->beginTransaction();
            try {
                $this->CreateDir($model->sub_files_tmp, 'sub_files');
                $model->sub_files = $this->uploadMultipleFile($model, null, 'sub_files');

                if ($model->save()) {

                    $ts->commit();
                    return Json::encode([
                        'title' => 'ผลการบันทึก',
                        'message' => 'บันทึกข้อมูลสำเร็จ',
                        'status' => 'success',
                        'url' => Url::to(['/meeting/meeting/view', 'id' => $agenda->agenda_topic_id]),
                        'id'=>$agenda->agenda_topic_id
                    ]);
                } else {
                    $ts->rollBack();
                    $error = $model->getErrorSummary(true);
                    return Json::encode([
                        'title' => 'ผลการบันทึกข้อมูล',
                        'message' => 'พบข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้ เนื่องจาก' . implode(',', $error),
                        'status' => 'error',
                    ]);
                }

                //return $this->redirect(['project/index']);
            } catch (\Exception $exception) {
                $ts->rollBack();
                $message = $exception->getMessage();
                return Json::encode([
                    'title' => 'ผลการบันทึก',
                    'message' => 'พบข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้ เนื่องจาก' . $message,
                    'status' => 'error',
                ]);
            }

        } else {
            $agenda = Agenda::findOne($id);
            $model->sub_files_tmp = substr(Yii::$app->getSecurity()->generateRandomString(), 10);
            $model->agenda_id = $id;
            $model->agenda_topic_id = $agenda->agenda_topic_id;

            return $this->renderAjax('_form',[
                'model'=> $model,
                'modelAgenda'=>$agenda
            ]);
        }
    }

    /**
     * Updates an existing SubAgenda model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $tmpFiles = $model->sub_files;
        if ($model->load(Yii::$app->request->post())) {
            $agenda = Agenda::findOne($model->agenda_id);
            $ts = Yii::$app->db->beginTransaction();
            try {
                $this->CreateDir($model->sub_files_tmp, 'sub_files');
                //print_r($model->sub_files);
                $model->sub_files = $this->uploadMultipleFile($model, $tmpFiles, 'sub_files');

//                print_r($model->attributes);
                if ($model->save()) {

                    $ts->commit();
                    return Json::encode([
                        'title' => 'ผลการบันทึก',
                        'message' => 'บันทึกข้อมูลสำเร็จ',
                        'status' => 'success',
                        'url' => Url::to(['/meeting/meeting/view', 'id' => $agenda->agenda_topic_id]),
                        'id'=>$agenda->agenda_topic_id
                    ]);
                } else {
                    $ts->rollBack();
                    $error = $model->getErrorSummary(true);
                    return Json::encode([
                        'title' => 'ผลการบันทึกข้อมูล',
                        'message' => 'พบข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้ เนื่องจาก' . implode(',', $error),
                        'status' => 'error',
                    ]);
                }

                //return $this->redirect(['project/index']);
            } catch (\Exception $exception) {
                $ts->rollBack();
                $message = $exception->getMessage();
                return Json::encode([
                    'title' => 'ผลการบันทึก',
                    'message' => 'พบข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้ เนื่องจาก' . $message,
                    'status' => 'error',
                ]);
            }

        } else {
            $agenda = Agenda::findOne($model->agenda_id);
            //$model->sub_files_tmp = substr(Yii::$app->getSecurity()->generateRandomString(), 10);
            //$model->agenda_id = $id;
            //$model->agenda_topic_id = $agenda->agenda_topic_id;

            return $this->renderAjax('_formUpdate',[
                'model'=> $model,
                'modelAgenda'=>$agenda
            ]);
        }
    }

    /**
     * Deletes an existing SubAgenda model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithRelated();

        return $this->redirect(['index']);
    }

    
    /**
     * Finds the SubAgenda model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SubAgenda the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SubAgenda::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for MtSubAgendaFile
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddMtSubAgendaFile()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('MtSubAgendaFile');
            if (!empty($row)) {
                $row = array_values($row);
            }
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formMtSubAgendaFile', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionValidateSubAgenda()
    {
        $data = Yii::$app->request->post('SubAgenda');
        if (null != $data['sub_agenda_id']) {
            $model = SubAgenda::findOne($data['sub_agenda_id']);
        } else {
            //ถ้าเป็นการบันทึกใหม่ ให้สร้าง โมเดลใหม่
            $model = new SubAgenda();
            //$budget = [new ProjectDetailBudget()];
            //$model->scenario = 'insert';
        }
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            //$model->_agenda_topic = Yii::$app->request->post('Meeting')['_agenda_topic'];

            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }
    }

    private function CreateDir($folderName, $attribute = 'sub_files')
    {
        //echo $folderName;
        $basePath = SubAgenda::getUploadPath('sub_files');

        if ($folderName != null && !is_dir($basePath . $folderName)) {

            BaseFileHelper::createDirectory($basePath . $folderName, 0777);
        }

        return;
    }

    public function actionDeletefile($id, $field, $fileName)
    {
        $status = ['success' => false];
        if (in_array($field, ['report_file', 'sub_files'])) {
            $model = $this->findModel($id);
            //echo  $field;
            $files = Json::decode($model->{$field});
            //print_r($files);
            //echo $fileName;
            if (array_key_exists($fileName, $files)) {
                //print_r($model->attributes);
                if ($this->deleteFile('files', $model->sub_files_tmp, $fileName, $field)) {
                    $status = ['success' => true];
                    unset($files[$fileName]);
                    $model->{$field} = Json::encode($files);
                    if (!$model->save(false)) {
                        print_r($model->getErrorSummary(true));
                    }
                }
            }
        }
        echo json_encode($status);
    }

    private function deleteFile($type = 'files', $ref, $fileName, $field)
    {

        if (in_array($type, ['files', 'thumbnail', 'other'])) {
            if (in_array($type, ['files', 'thumbnail'])) {
                $filePath = SubAgenda::getUploadPath($field) . $ref . '/' . $fileName;
//                $fileTPath = MeetingReport::getUploadPath($field) . $ref . '/thumbnail/' . $fileName;
                @unlink($filePath);
//                @unlink($fileTPath);
            } else {
                $filePath = SubAgenda::getUploadPath($field) . $ref . '/' . $fileName;
                @unlink($filePath);
            }

            return true;
        } else {
            return false;
        }
    }

    private function uploadMultipleFile($model, $tempFile = null, $attribute)
    {
        //echo $attribute;
        $files = [];
        $json = '';
//        var_dump($tempFile);
//        if($tempFile!=null){
            $tempFile = Json::decode($tempFile);
//        }

        $UploadedFiles = UploadedFile::getInstances($model, $attribute);

//        print_r($UploadedFiles);
//        print_r($tempFile);

        if ($UploadedFiles!==null) {
//            echo 1;
            foreach ($UploadedFiles as $file) {
//                echo 2;
                try {
                    $oldFileName = $file->basename . '.' . $file->extension;
                    $newFileName = md5($file->basename . time()) . '.' . $file->extension;

                    $file->saveAs(SubAgenda::getUploadPath($attribute) . $model->sub_files_tmp . '/' .
                        $newFileName);

                    //$fileThumbnailPath = Project::getUploadPath($attribute) . $model->{$attribute . '_ref'} . '/thumbnail';

                    /*@Image::getImagine()->open(Project::getUploadPath($attribute) .
                        $model->{$attribute . '_ref'} . '/' . $newFileName)->thumbnail(new Box(120, 120))->save($fileThumbnailPath . '/' . $newFileName, ['quality' => 90]);*/

                    $files[$newFileName] = $oldFileName;
                } catch (\Exception $e) {
                    throw new \Exception($e->getMessage());
                }
            }
            $json = Json::encode(ArrayHelper::merge($tempFile, $files));
        } else {
            $json =$tempFile;
        }
//        print_r($json);
        return $json;
    }

    /**
     * @param $id
     * @param $file
     * @param $file_name
     * @param string $type
     * @throws NotFoundHttpException
     * download file manual only
     */
    public function actionDownload($id, $file, $file_name, $field)
    {
        //echo $id.' '.$file.' '.$file_name;
        $model = $this->findModel($id);
        if (!empty($model->{$field})) {
            Yii::$app->response->sendFile($model->getUploadPath($field) . '' . $model->sub_files_tmp . '/' . $file, $file_name, ['inline' => true]);
        }
    }

    public function actionDownloadZip($id, $type = 'agenda_files')
    {

        $model = $this->findModel($id);
        $path = $model->agenda_files_tmp;
        $rootPath = ($model::getUploadUrl() . $path);
        $downloadPath = $model::getUploadPath() . $path;

        //echo $downloadPath.'.zip';
//        exit();
        if (is_file($downloadPath . '.zip')) {
            return Yii::$app->response->sendFile($downloadPath . '.zip', 'download.zip', ['inline' => true]);
            //echo 1;
            exit();
        }

        $zip = new ZipArchive();
        $filename = "$downloadPath" . ".zip";

        if ($zip->open($filename, ZipArchive::CREATE) !== TRUE) {
            exit("cannot open <$filename>\n");
        }

        $all = Json::decode($model->$type);
        foreach ($all as $key => $item) {
//            echo $item.' '.$key;
            $file = $item;
            $relPath = $model::getUploadPath($type) . $model->report_path_temp . '/' . $key;
            $zip->addFile($relPath, $file);
        }

        $zip->close();

        return Yii::$app->response->sendFile($model->getUploadPath() . $model->agenda_files_tmp . '.zip', 'download.zip', ['inline' => true]);
    }
}

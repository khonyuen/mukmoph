<?php

namespace frontend\modules\controllers;

use common\models\AgendaTopic;
use common\models\TopicType;
use Yii;
use common\models\Agenda;
use yii\bootstrap4\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseFileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;
use ZipArchive;

/**
 * AgendaController implements the CRUD actions for Agenda model.
 */
class AgendaController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'index', 'view', 'create', 'update',
                            'delete', 'pdf', 'add-mt-agenda',
                            'add-mt-agenda-topic', 'validate-agenda',
                            'delete-agenda-topic',
                            'add-agenda-topic',
                            'download','download-zip'
                        ],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'view', 'download','download-zip'
                        ],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => false,
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Agenda models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Agenda::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Agenda model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $providerMtSubAgenda = new \yii\data\ArrayDataProvider([
            'allModels' => $model->mtSubAgendas,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerMtSubAgenda' => $providerMtSubAgenda,
        ]);
    }

    /**
     * Creates a new Agenda model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($agenda_topic_id=null,$meeting_id=null,$topic_id=null)
    {
        $model = new Agenda();

        if ($model->load(Yii::$app->request->post())) {

            $ts = Yii::$app->db->beginTransaction();
            try {
                $this->CreateDir($model->agenda_files_tmp, 'agenda_files');
                $model->agenda_files = $this->uploadMultipleFile($model, null, 'agenda_files');

                if ($model->save()) {

                    $ts->commit();
                    return Json::encode([
                        'title' => 'ผลการบันทึก',
                        'message' => 'บันทึกข้อมูลสำเร็จ',
                        'status' => 'success',
                        'url' => Url::to(['/meeting/meeting/view', 'id' => $agenda_topic_id]),
                        'id'=>$agenda_topic_id
                    ]);
                } else {
                    $ts->rollBack();
                    $error = $model->getErrorSummary(true);
                    return Json::encode([
                        'title' => 'ผลการบันทึกข้อมูล',
                        'message' => 'พบข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้ เนื่องจาก' . implode(',', $error),
                        'status' => 'error',
                    ]);
                }

                //return $this->redirect(['project/index']);
            } catch (\Exception $exception) {
                $ts->rollBack();
                $message = $exception->getMessage();
                return Json::encode([
                    'title' => 'ผลการบันทึก',
                    'message' => 'พบข้อผิดพลาด ไม่สามารถบันทึกข้อมูลได้ เนื่องจาก' . $message,
                    'status' => 'error',
                ]);
            }

        } else {
            $topicType = TopicType::findOne($topic_id);
            $agendaTopic = AgendaTopic::findOne($agenda_topic_id);
            $model->agenda_files_tmp = substr(Yii::$app->getSecurity()->generateRandomString(), 10);
            $model->agenda_topic_type = $topic_id;
            $model->agenda_topic_id = $agenda_topic_id;
            $model->meeting_id = $meeting_id;
            return $this->renderAjax('_form',[
                'model'=> $model,
                'modelAT'=> $agendaTopic,
                'modelTT'=> $topicType,
            ]);
        }
    }

    /**
     * Updates an existing Agenda model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $tmpFiles = $model->agenda_files;
        if ($model->load(Yii::$app->request->post())) {

            $ts = Yii::$app->db->beginTransaction();
            try {
                $this->CreateDir($model->agenda_files_tmp, 'agenda_files');

                $model->agenda_files = $this->uploadMultipleFile($model, $tmpFiles, 'agenda_files');

                if ($model->save()) {

                    $ts->commit();
                    return Json::encode([
                        'title' => 'ผลการบันทึก',
                        'message' => 'แก้ไขข้อมูลสำเร็จ',
                        'status' => 'success',
                        'url' => Url::to(['/meeting/meeting/view', 'id' => $model->agenda_topic_id]),
                        'id'=>$model->agenda_topic_id
                    ]);
                } else {
                    $ts->rollBack();
                    $error = $model->getErrorSummary(true);
                    return Json::encode([
                        'title' => 'ผลการบันทึกข้อมูล',
                        'message' => 'พบข้อผิดพลาด ไม่สามารถแก้ไขข้อมูลได้ เนื่องจาก' . implode(',', $error),
                        'status' => 'error',
                    ]);
                }

                //return $this->redirect(['project/index']);
            } catch (\Exception $exception) {
                $ts->rollBack();
                $message = $exception->getMessage();
                return Json::encode([
                    'title' => 'ผลการบันทึก',
                    'message' => 'พบข้อผิดพลาด ไม่สามารถแก้ไขข้อมูลได้ เนื่องจาก' . $message,
                    'status' => 'error',
                ]);
            }

        } else {
            $topicType = TopicType::findOne($model->agenda_topic_type);
            $agendaTopic = AgendaTopic::findOne($model->agenda_topic_id);
            //$model->agenda_files_tmp = substr(Yii::$app->getSecurity()->generateRandomString(), 10);
            //$model->agenda_topic_type = $topic_id;
            //$model->agenda_topic_id = $agenda_topic_id;
            //$model->meeting_id = $meeting_id;
            return $this->renderAjax('_formUpdate',[
                'model'=> $model,
                'modelAT'=> $agendaTopic,
                'modelTT'=> $topicType,
            ]);
        }
    }

    /**
     * Deletes an existing Agenda model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        try {
            $ts = Yii::$app->db->beginTransaction();
            $path = $model->agenda_files_tmp;
            //$res_id =$model->responsible_id;
            if ($model->delete()) {
                // remove file
                $this->removeUploadDir($model, 'agenda_files', $path);
                $ts->commit();
                return Json::encode([
                    'result' => true,
                    'msg' => 'ลบข้อมูลเรียบร้อย',
                    'url'=>Url::to(['/meeting/meeting/view','id'=>$model->agenda_topic_id]),
                    'id'=>$model->agenda_topic_id,
                ]);
            }
        } catch (Exception $exception) {
            $ts->rollBack();
            return Json::encode([
                'result' => false,
                'msg' => 'ไม่สามารถลบข้อมูลได้ เนื่องจาก ' . $exception->getMessage(),
                'options' => [],
            ]);
        }
    }

    private function removeUploadDir($model, $attribute, $path)
    {
        if ($attribute != '') {
            BaseFileHelper::removeDirectory(Agenda::getUploadPath($attribute) . $path);
        }
    }

    
    /**
     * Finds the Agenda model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Agenda the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Agenda::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for MtSubAgenda
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddMtSubAgenda()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('MtSubAgenda');
            if (!empty($row)) {
                $row = array_values($row);
            }
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formMtSubAgenda', ['row' => $row]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionValidateAgenda()
    {
        $data = Yii::$app->request->post('Agenda');
        if (null != $data['agenda_id']) {
            $model = Agenda::findOne($data['agenda_id']);
        } else {
            //ถ้าเป็นการบันทึกใหม่ ให้สร้าง โมเดลใหม่
            $model = new Agenda();
            //$budget = [new ProjectDetailBudget()];
            //$model->scenario = 'insert';
        }
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            //$model->_agenda_topic = Yii::$app->request->post('Meeting')['_agenda_topic'];

            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }
    }

    private function CreateDir($folderName, $attribute = 'agenda_files_tmp')
    {
        //echo $folderName;
        $basePath = Agenda::getUploadPath('agenda_files');

        if ($folderName != null && !is_dir($basePath . $folderName)) {

            BaseFileHelper::createDirectory($basePath . $folderName, 0777);
        }

        return;
    }

    private function uploadMultipleFile($model, $tempFile = null, $attribute)
    {
        //echo $attribute;
        $files = [];
        $json = '';
        $tempFile = Json::decode($tempFile);
        $UploadedFiles = UploadedFile::getInstances($model, $attribute);

        //print_r($UploadedFiles);
//        var_dump(empty($UploadedFiles));
        //print_r($tempFile);
//        var_dump($tempFile);
        if ($UploadedFiles!==null) {
            foreach ($UploadedFiles as $file) {
                try {
                    $oldFileName = $file->basename . '.' . $file->extension;
                    $newFileName = md5($file->basename . time()) . '.' . $file->extension;

                    $file->saveAs(Agenda::getUploadPath($attribute) . $model->agenda_files_tmp . '/' .
                        $newFileName);

                    //$fileThumbnailPath = Project::getUploadPath($attribute) . $model->{$attribute . '_ref'} . '/thumbnail';

                    /*@Image::getImagine()->open(Project::getUploadPath($attribute) .
                        $model->{$attribute . '_ref'} . '/' . $newFileName)->thumbnail(new Box(120, 120))->save($fileThumbnailPath . '/' . $newFileName, ['quality' => 90]);*/

                    $files[$newFileName] = $oldFileName;
                } catch (\Exception $e) {
                    throw new \Exception($e->getMessage());
                }
            }
            $json = Json::encode(ArrayHelper::merge($tempFile, $files));
        } else {
//            print_r($tempFile);
            $json = $tempFile;
        }
//        print_r($json);
        return $json;
    }

    /**
     * @param $id
     * @param $file
     * @param $file_name
     * @param string $type
     * @throws NotFoundHttpException
     * download file manual only
     */
    public function actionDownload($id, $file, $file_name, $field)
    {
        //echo $id.' '.$file.' '.$file_name;
        $model = $this->findModel($id);
        if (!empty($model->{$field})) {
            Yii::$app->response->sendFile($model->getUploadPath($field) . '' . $model->agenda_files_tmp . '/' . $file, $file_name, ['inline' => true]);
        }
    }

    public function actionDownloadZip($id, $type = 'agenda_files')
    {

        $model = $this->findModel($id);
        $path = $model->agenda_files_tmp;
        $rootPath = ($model::getUploadUrl() . $path);
        $downloadPath = $model::getUploadPath() . $path;

        //echo $downloadPath.'.zip';
//        exit();
        if (is_file($downloadPath . '.zip')) {
            return Yii::$app->response->sendFile($downloadPath . '.zip', 'download.zip', ['inline' => true]);
            //echo 1;
            exit();
        }

        $zip = new ZipArchive();
        $filename = "$downloadPath" . ".zip";

        if ($zip->open($filename, ZipArchive::CREATE) !== TRUE) {
            exit("cannot open <$filename>\n");
        }

        $all = Json::decode($model->$type);
        foreach ($all as $key => $item) {
//            echo $item.' '.$key;
            $file = $item;
            $relPath = $model::getUploadPath($type) . $model->report_path_temp . '/' . $key;
            $zip->addFile($relPath, $file);
        }

        $zip->close();

        return Yii::$app->response->sendFile($model->getUploadPath() . $model->agenda_files_tmp . '.zip', 'download.zip', ['inline' => true]);
    }
}

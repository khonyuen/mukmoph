<?php

use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $content string
 */

$this->beginContent('@t02/views/layouts/_base_home.php');
?>

<?= $this->render('_header_mobile') ?>

<?= $this->render('_content_home',['content'=>$content]) ?>

<?= $this->render('_quick_user',['content'=>$content]) ?>

<?= $this->render('_quick_panel',['content'=>$content]) ?>

<?= $this->render('_scrollpage') ?>

<?php
$this->endContent();
?>
<?php
/**
 * @var $this yii\web\View
 * @var $content string
 */

$drAsset = Yii::$app->assetManager->getPublishedUrl('@t02/dist');
$toolbars = [];

?>

<div class="d-flex flex-column flex-root">
    <!--begin::Page-->
    <div class="d-flex flex-row flex-column-fluid page">
        <!--begin::Wrapper-->
        <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
            <!--begin::Header-->
            <div id="kt_header" class="header header-fixed">
                <!--begin::Container-->
                <div class="container d-flex align-items-stretch justify-content-between">
                    <!--begin::Left-->
                    <?= $this->render('_top_left_menu'); ?>
                    <!--end::Left-->

                    <!--begin::Topbar-->
                    <?= $this->render('_top_right_menu') ?>
                    <!--end::Topbar-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Header-->
            <!--begin::Content-->
            <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                <!--begin::Subheader-->
                <div class="subheader py-2 py-lg-12 subheader-transparent" id="kt_subheader">
                    <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                        <?= $this->render('_breadscrumbs') ?>
                        <!--end::Subheader-->
                        <!--begin::Toolbar-->
                        <?= $this->render('_toolbar', [
                            'toolbar' => $this->params['toolbar'],
                        ]) ?>
                    </div>
                </div>
                <!--end::Toolbar-->
                <!--begin::Entry-->
                <div class="d-flex flex-column-fluid">
                    <!--begin::Container-->
                    <div class="container">
                        <?= $content ?>
                    </div>
                    <!--end::Container-->
                </div>
                <!--end::Entry-->
            </div>
            <!--end::Content-->
            <!--begin::Footer-->
            <?= $this->render('_footer') ?>
            <!--end::Footer-->
        </div>
        <!--end::Wrapper-->
    </div>
    <!--end::Page-->
</div>
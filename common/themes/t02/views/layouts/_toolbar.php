<?php
/**
 * @var $this yii\web\View
 * @var $toolbar array|string
 * type [hr,button],
 *  hr
 *  button, link,
 *      sub_button,sub_link,sub_label
 */

//var_dump($this->params['toolbar']);
//
//var_dump($this->params['breadcrumbs']);
//var_dump($toolbar);
if (is_array($toolbar) || $toolbar != '' || $toolbar != []) {
    ?>
    <div class="d-flex align-items-center">
    <?php
    if (is_array($toolbar)) {
        foreach ($toolbar as $item) {
            //print_r($item);
            //echo $item['type'];
            //var_dump($item['sub_button']);
            if ($item['type'] == 'button' && isset($item['sub_button'])) {
                ?>
                <div class="dropdown dropdown-inline ml-2" data-toggle="tooltip" title="Quick actions" data-placement="top">
                    <a href="#" class="btn btn-<?=$item['style']?> font-weight-bold py-3 px-6" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false"><?=$item['label']?></a>
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-md dropdown-menu-right">
                        <!--begin::Navigation-->
                        <ul class="navi navi-hover py-5">
                            <?php
                            foreach ($item['sub_button'] as $sub_item) {
                                ?>
                                <!--begin::Dropdown-->
                                <li class="navi-item">
                                    <a href="<?= $sub_item['link'] ?>" class="navi-link">
														<span class="navi-icon">
															<i class="<?= $sub_item['icon'] ?>"></i>
														</span>
                                        <span class="navi-text"><?= $sub_item['label'] ?></span>
                                    </a>
                                </li>

                                <?php
                            }
                            ?>
                        </ul>
                        <!--end::Navigation-->
                    </div>
                </div>
                <!--end::Dropdown-->
                <?php
            }
        }
        ?>
        </div>
        <?php
    } else {
        echo "";
    }
} else {
    echo "";
}
?>


<!--<div class="d-flex align-items-center">

    <a href="#" class="btn btn-transparent-white font-weight-bold py-3 px-6 mr-2">Reports</a>

    <div class="dropdown dropdown-inline ml-2" data-toggle="tooltip" title="Quick actions" data-placement="top">
        <a href="#" class="btn btn-white font-weight-bold py-3 px-6" data-toggle="dropdown" aria-haspopup="true"
           aria-expanded="false">Actions</a>
        <div class="dropdown-menu p-0 m-0 dropdown-menu-md dropdown-menu-right">

            <ul class="navi navi-hover py-5">
                <li class="navi-item">
                    <a href="#" class="navi-link">
														<span class="navi-icon">
															<i class="flaticon2-drop"></i>
														</span>
                        <span class="navi-text">New Group</span>
                    </a>
                </li>
                <li class="navi-item">
                    <a href="#" class="navi-link">
														<span class="navi-icon">
															<i class="flaticon2-list-3"></i>
														</span>
                        <span class="navi-text">Contacts</span>
                    </a>
                </li>
                <li class="navi-item">
                    <a href="#" class="navi-link">
														<span class="navi-icon">
															<i class="flaticon2-rocket-1"></i>
														</span>
                        <span class="navi-text">Groups</span>
                        <span class="navi-link-badge">
															<span class="label label-light-primary label-inline font-weight-bold">new</span>
														</span>
                    </a>
                </li>
                <li class="navi-item">
                    <a href="#" class="navi-link">
														<span class="navi-icon">
															<i class="flaticon2-bell-2"></i>
														</span>
                        <span class="navi-text">Calls</span>
                    </a>
                </li>
                <li class="navi-item">
                    <a href="#" class="navi-link">
														<span class="navi-icon">
															<i class="flaticon2-gear"></i>
														</span>
                        <span class="navi-text">Settings</span>
                    </a>
                </li>
                <li class="navi-separator my-3"></li>
                <li class="navi-item">
                    <a href="#" class="navi-link">
														<span class="navi-icon">
															<i class="flaticon2-magnifier-tool"></i>
														</span>
                        <span class="navi-text">Help</span>
                    </a>
                </li>
                <li class="navi-item">
                    <a href="#" class="navi-link">
														<span class="navi-icon">
															<i class="flaticon2-bell-2"></i>
														</span>
                        <span class="navi-text">Privacy</span>
                        <span class="navi-link-badge">
															<span class="label label-light-danger label-rounded font-weight-bold">5</span>
														</span>
                    </a>
                </li>
            </ul>

        </div>
    </div>

</div>
-->
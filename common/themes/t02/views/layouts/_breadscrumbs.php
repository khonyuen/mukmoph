<?php

use yii\widgets\Breadcrumbs; ?>
        <!--begin::Info-->
        <div class="d-flex align-items-center flex-wrap mr-1">

            <!--begin::Heading-->
            <div class="d-flex flex-column">


                <!--begin::Title-->
                <h2 class="text-white font-weight-bold my-2 mr-5"><?=$this->title?></h2>
                <!--end::Title-->
                <!--begin::Breadcrumb-->
                <div class="d-flex align-items-center font-weight-bold my-2">
                    <?php
                    echo Breadcrumbs::widget([
                        'homeLink' => [
                            'label' => '<i class="flaticon2-shelter text-white icon-1x"></i> <span class="label label-dot label-sm bg-white opacity-75 mx-3"></span>',
                            'url' => '#',
                            'class' => 'opacity-75 hover-opacity-100',
                            'encode' => false,
                        ],
                        'links' => isset($this->params['breadcrumbs']) ?
                            $this->params['breadcrumbs'] : [],
                        'options' => [
                            //'class' => 'kt-subheader__breadcrumbs',
                        ],
                        'tag' => false,
                        'itemTemplate' => "{link} \n",
                        'activeItemTemplate' => "<a class='text-white text-hover-white opacity-75 hover-opacity-100' href='javascript:;'>{link}</a>\n"
                    ]);
                    ?>
                </div>
                <!--end::Breadcrumb-->
            </div>
            <!--end::Heading-->
        </div>
        <!--end::Info-->
        <!--begin::Toolbar-->
        <!-- //// -->
        <!--end::Toolbar-->

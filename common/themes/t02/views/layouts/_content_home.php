<?php
/**
 * @var $this yii\web\View
 * @var $content string
 */

$drAsset = Yii::$app->assetManager->getPublishedUrl('@t02/dist');
$toolbars = [];

?>

<div class="d-flex flex-column flex-root">
    <!--begin::Page-->
    <div class="d-flex flex-row flex-column-fluid page">
        <!--begin::Wrapper-->
        <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
            <!--begin::Header-->
            <div id="kt_header" class="header header-fixed">
                <!--begin::Container-->
                <div class="container d-flex align-items-stretch justify-content-between">
                    <!--begin::Left-->
                    <?= $this->render('_top_left_menu'); ?>
                    <!--end::Left-->

                    <!--begin::Topbar-->
                    <?= $this->render('_top_right_menu') ?>
                    <!--end::Topbar-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Header-->
            <!--begin::Content-->
            <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                <?= $content ?>
            </div>
            <!--end::Content-->
            <!--begin::Footer-->
            <?= $this->render('_footer') ?>
            <!--end::Footer-->
        </div>
        <!--end::Wrapper-->
    </div>
    <!--end::Page-->
</div>
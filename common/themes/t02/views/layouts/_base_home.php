<?php
/* @var $this \yii\web\View */

/* @var $content string */

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@t02/dist');

use t02\assets\MainAsset;
use yii\bootstrap4\Modal;
use yii\helpers\Html; ?>
<?php
MainAsset::register($this);
$this->beginPage();
?>
<html lang="en">
<!--begin::Head-->
<head>
    <base href="">
    <meta charset="utf-8" />
    <title><?=Html::encode($this->title) ?></title>
    <meta name="description" content="<?=Yii::$app->name ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="canonical" href="https://www.mdo.moph.go.th" />
    <?php $this->registerCsrfMetaTags() ?>

    <?php $this->head(); ?>
</head>
<!--end::Head-->
<!--begin::Body-->
<body  id="kt_body" style="background-image: url(<?=$directoryAsset?>/assets/media/bg/bg-10.jpg)" class="quick-panel-right demo-panel-right offcanvas-right header-fixed page-loading">
<?=$content?>

<?php
\yii\bootstrap4\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal_main',
    //'size' => 'modal-lg',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE],
//    'id' => 'modal-lov-dept',
//    'options' => [
//        'data-backdrop' => 'static', 'data-keyboard' => FALSE,
//    ],
    'size' => Modal::SIZE_EXTRA_LARGE,
    'options' => [
        'max-width' => '98%',
        'data-backdrop' => 'static', 'data-keyboard' => FALSE,
    ],
    //'header' => '',
    'class' => 'modal',
    'footer' => Html::button('<i class="fa fa-close"></i> ยกเลิก', ['data-dismiss' => "modal", 'class' => 'btn btn-danger lovClose']),
]);

echo "<div id='modalContent'></div>";
//echo "<div id='modalContent'>{$this->render("//department/index")}</div>";
//{$this->render("//department/_departmentLOV")}
?>

<?php
\yii\bootstrap4\Modal::end();
?>
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>

<?php
use yii\helpers\Url;

$drAsset = Yii::$app->assetManager->getPublishedUrl('@t02/dist');

//echo Url::current([],false);
// menu-item-open menu-item-here
// menu-item-active
?>

<div class="d-flex align-items-stretch mr-3">
    <!--begin::Header Logo-->
    <div class="header-logo">
        <a href="index.html">
            <img alt="Logo" src="<?= $drAsset ?>/assets/media/logos/logo-4.png" class="logo-default max-h-150px"/>
            <img alt="Logo" src="<?= $drAsset ?>/assets/media/logos/logo-4-sm.png" class="logo-sticky max-h-100px"/>
        </a>
    </div>
    <!--end::Header Logo-->
    <!--begin::Header Menu Wrapper-->
    <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
        <!--begin::Header Menu-->
        <div id="kt_header_menu" class="header-menu header-menu-left header-menu-mobile header-menu-layout-default">
            <!--begin::Header Nav-->
            <ul class="menu-nav">
                <li class="menu-item menu-item-submenu menu-item-rel "
                    data-menu-toggle="click" aria-haspopup="true">
                    <a href="javascript:;" class="menu-link menu-toggle">
                        <span class="menu-text">หน้าหลัก</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu menu-submenu-classic menu-submenu-left">
                        <ul class="menu-subnav">
                            <li class="menu-item " aria-haspopup="true">
                                <a href="<?=Url::to(['/site/index'])?>" class="menu-link">
                                    <span class="menu-text">สสจ.มุกดาหาร</span>
                                    <span class="menu-desc"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="menu-item menu-item-submenu menu-item-rel" data-menu-toggle="click" aria-haspopup="true">
                    <a href="javascript:;" class="menu-link menu-toggle">
                        <span class="menu-text">ระบบการประชุม</span>
                        <span class="menu-desc"></span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu menu-submenu-classic menu-submenu-left">
                        <ul class="menu-subnav">
                            <li class="menu-item" aria-haspopup="true">
                                <a href="<?=Url::to(['/meeting/default/index'])?>"
                                   class="menu-link">
																<span class="svg-icon menu-icon">
																	<!--begin::Svg Icon | path:assets/media/svg/icons/Home/Library.svg-->
																	<svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px"
                                                                         viewBox="0 0 24 24" version="1.1">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																			<rect x="0" y="0" width="24" height="24"/>
																			<path d="M5,3 L6,3 C6.55228475,3 7,3.44771525 7,4 L7,20 C7,20.5522847 6.55228475,21 6,21 L5,21 C4.44771525,21 4,20.5522847 4,20 L4,4 C4,3.44771525 4.44771525,3 5,3 Z M10,3 L11,3 C11.5522847,3 12,3.44771525 12,4 L12,20 C12,20.5522847 11.5522847,21 11,21 L10,21 C9.44771525,21 9,20.5522847 9,20 L9,4 C9,3.44771525 9.44771525,3 10,3 Z"
                                                                                  fill="#000000"/>
																			<rect fill="#000000" opacity="0.3"
                                                                                  transform="translate(17.825568, 11.945519) rotate(-19.000000) translate(-17.825568, -11.945519)"
                                                                                  x="16.3255682" y="2.94551858" width="3" height="18"
                                                                                  rx="1"/>
																		</g>
																	</svg>
                                                                    <!--end::Svg Icon-->
																</span>
                                    <span class="menu-text">วาระการประชุม (กวป. กบห.)</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </li>

            </ul>
            <!--end::Header Nav-->
        </div>
        <!--end::Header Menu-->
    </div>
    <!--end::Header Menu Wrapper-->
</div>

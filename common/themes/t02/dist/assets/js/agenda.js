$(document).on("beforeSubmit", "form#create-agenda", function (e) {
    e.preventDefault();
    e.isImmediatePropagationStopped();
    var _this = $(this);
    //var form = document.getElementById('baseForm');
    var formData = new FormData($(this)[0]);

    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'กำลังบันทึกข้อมูล...'
    });

    $.ajax({
        cache: false,
        contentType: false,
        processData: false,
        url: $(this).attr("action"),
        type: "post",
        data: formData,//form.serialize(),
        success: function (response) {
            // do something with response
            console.log(response);
            //unblockUi('blockPanel');
            response = JSON.parse(response);
            swal.fire(
                response.title,
                response.message,
                response.status
            );
            if (response.status == 'success') {
                // var id_pjax = _this.closest('.table-responsive.kv-grid-container').attr('id');
                // if(id_pjax){
                //     id_pjax = id_pjax.replace('container','pjax');
                $("form#create-agenda")[0].reset();
                $.pjax.reload({container: '#wr-' + response.id});


                //     console.log('reload '+id_pjax);
                // }
            }

            KTApp.unblockPage();
        },
        complete: function () {
            //unblockUi('blockPanel');
            KTApp.unblockPage();
        },
        error: function (error) {
            //unblockUi('blockPanel');
            console.info(error);
            KTApp.unblockPage();
        }
    });

    return false;
});
$(document).on("beforeSubmit", "form#create-sub-agenda", function (e) {
    e.preventDefault();
    e.isImmediatePropagationStopped();
    var _this = $(this);
    //var form = document.getElementById('baseForm');
    var formData = new FormData($(this)[0]);

    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'กำลังบันทึกข้อมูล...'
    });

    $.ajax({
        cache: false,
        contentType: false,
        processData: false,
        url: $(this).attr("action"),
        type: "post",
        data: formData,//form.serialize(),
        success: function (response) {
            // do something with response
            console.log(response);
            //unblockUi('blockPanel');
            response = JSON.parse(response);
            swal.fire(
                response.title,
                response.message,
                response.status
            );
            if (response.status == 'success') {
                // var id_pjax = _this.closest('.table-responsive.kv-grid-container').attr('id');
                // if(id_pjax){
                //     id_pjax = id_pjax.replace('container','pjax');
                $("form#create-sub-agenda")[0].reset();
                $.pjax.reload({container: '#wr-' + response.id});


                //     console.log('reload '+id_pjax);
                // }
            }

            KTApp.unblockPage();
        },
        complete: function () {
            //unblockUi('blockPanel');
            KTApp.unblockPage();
        },
        error: function (error) {
            //unblockUi('blockPanel');
            console.info(error);
            KTApp.unblockPage();
        }
    });

    return false;
});

$(document).on("beforeSubmit", "form#update-agenda", function (e) {
    e.preventDefault();
    e.isImmediatePropagationStopped();
    var _this = $(this);
    //var form = document.getElementById('baseForm');
    var formData = new FormData($(this)[0]);

    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'กำลังแก้ไขข้อมูล...'
    });

    $.ajax({
        cache: false,
        contentType: false,
        processData: false,
        url: $(this).attr("action"),
        type: "post",
        data: formData,//form.serialize(),
        success: function (response) {
            // do something with response
            console.log(response);
            //unblockUi('blockPanel');
            response = JSON.parse(response);
            swal.fire(
                response.title,
                response.message,
                response.status
            );
            if (response.status == 'success') {
                // var id_pjax = _this.closest('.table-responsive.kv-grid-container').attr('id');
                // if(id_pjax){
                //     id_pjax = id_pjax.replace('container','pjax');
                $.pjax.reload({container: '#wr-' + response.id});
                $("#modal_main").modal('hide');

                //     console.log('reload '+id_pjax);
                // }
            }

            KTApp.unblockPage();
        },
        complete: function () {
            //unblockUi('blockPanel');
            KTApp.unblockPage();
        },
        error: function (error) {
            //unblockUi('blockPanel');
            console.info(error);
            KTApp.unblockPage();
        }
    });

    return false;
});
$(document).on("beforeSubmit", "form#update-sub-agenda", function (e) {
    e.preventDefault();
    e.isImmediatePropagationStopped();
    var _this = $(this);
    //var form = document.getElementById('baseForm');
    var formData = new FormData($(this)[0]);

    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'กำลังแก้ไขข้อมูล...'
    });

    $.ajax({
        cache: false,
        contentType: false,
        processData: false,
        url: $(this).attr("action"),
        type: "post",
        data: formData,//form.serialize(),
        success: function (response) {
            // do something with response
            console.log(response);
            //unblockUi('blockPanel');
            response = JSON.parse(response);
            swal.fire(
                response.title,
                response.message,
                response.status
            );
            if (response.status == 'success') {
                // var id_pjax = _this.closest('.table-responsive.kv-grid-container').attr('id');
                // if(id_pjax){
                //     id_pjax = id_pjax.replace('container','pjax');
                $.pjax.reload({container: '#wr-' + response.id});
                $("#modal_main").modal('hide');

                //     console.log('reload '+id_pjax);
                // }
            }

            KTApp.unblockPage();
        },
        complete: function () {
            //unblockUi('blockPanel');
            KTApp.unblockPage();
        },
        error: function (error) {
            //unblockUi('blockPanel');
            console.info(error);
            KTApp.unblockPage();
        }
    });

    return false;
});

$(document).on('click','.deleteAgenda',function (e) {
    e.preventDefault();
    var url = $(this).data('url');

    swal.fire({
        title: 'ยืนยันการลบข้อมูล ?',
        text: "คุณต้องการลบข้อมูลนี้ !",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'ใช่, ลบมันซะ!'
    }).then(function (result) {
        if (result.value) {
            $.ajax({
                url: url,
                type: "post",
                //data: {key: id},
                success: function (response) {
                    var res = JSON.parse(response);
                    console.log(res.msg);

                    if(res.result) {
                        swal.fire(
                            'ลบข้อมูลเรียบร้อย!',
                            'success');

                        $.pjax.reload({container: '#wr-' + res.id});
                    }else {
                        swal.fire(
                            'ไม่สามารถลบได้! '+res.msg,
                            'warning');
                    }
                },
                error: function (error) {
                    console.log(error)
                }
            });
        }
    });
});
'use strict';

// Class definition
var KTDualListbox = function () {
    // Private functions

    var demo_insert = function () {
        // Dual Listbox
        var _this = document.getElementById('_agenda_topic_create');

        // init dual listbox
        var dualListBox = new DualListbox(_this, {
            addEvent: function (value) {
                console.log(value);
                //insertDb(value);
            },
            removeEvent: function (value) {
                console.log(value);
                //removeDb(value);
            },
            availableTitle: "วาระทั้งหมด",
            selectedTitle: "วาระที่เลือก",
            addButtonText: "<i class='flaticon2-next'></i>",
            removeButtonText: "<i class='flaticon2-back'></i>",
            addAllButtonText: "<i class='flaticon2-fast-next'></i>",
            removeAllButtonText: "<i class='flaticon2-fast-back'></i>",

        });
        dualListBox.search.classList.add('dual-listbox__search--hidden');
    };

    return {
        // public functions
        init: function () {
            demo_insert();
        },
    };
}();

$(document).on("beforeSubmit", "form#create-meeting", function (e) {
    e.preventDefault();
    e.isImmediatePropagationStopped();

    //var form = document.getElementById('baseForm');
    var formData = new FormData($(this)[0]);
    KTApp.blockPage({
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'กำลังบันทึกข้อมูล...'
    });
    $.ajax({
        cache: false,
        contentType: false,
        processData: false,
        url: $(this).attr("action"),
        type: "post",
        data: formData,//form.serialize(),
        success: function (response) {
            // do something with response
            console.log(response);
            //unblockUi('blockPanel');
            response = JSON.parse(response);
            if(response.status=='success') {
                swal.fire({
                    'title': response.title,
                    'text': response.message,
                    'icon': response.status,
                    'confirmButtonText': 'ตกลง',
                }).then((result) => {
                    /* Read more about isConfirmed, isDenied below */
                    if (result.isConfirmed) {
                        window.location.href = '' + response.url;
                    }
                });
            }else {
                swal.fire({
                    'title': response.title,
                    'text': response.message,
                    'icon': response.status,
                    //'confirmButtonText': 'ตกลง',
                })
            }
            KTApp.unblockPage();
        },
        complete: function () {
            //unblockUi('blockPanel');
            KTApp.unblockPage();
        },
        error: function (error) {
            //unblockUi('blockPanel');
            console.info(error);
            KTApp.unblockPage();
        }
    });

    return false;
});

window.addEventListener('load', function(){
    KTDualListbox.init();
});
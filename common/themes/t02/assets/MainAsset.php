<?php
namespace t02\assets;

use yii\web\AssetBundle;

class MainAsset extends AssetBundle
{
    public $sourcePath = '@t02/dist/';
    public $css =[
//        'https://fonts.googleapis.com/css2?family=Kanit&display=swap',
//        'https://fonts.googleapis.com/css2?family=Trirong&display=swap',
    'https://fonts.googleapis.com/css2?family=K2D:wght@500&display=swap',
        (YII_ENV_PROD) ? './minify/plugins/custom/fullcalendar/fullcalendar.bundle.css' : 'assets/plugins/custom/fullcalendar/fullcalendar.bundle.css',
        'assets/plugins/global/plugins.bundle.css',
        'assets/plugins/custom/prismjs/prismjs.bundle.css',
        'assets/css/style.bundle.css',
        'assets/css/custom.css',
        ['assets/media/logos/favicon.ico', 'rel' => 'shortcut icon'],
    ];

    public $js = [
        'assets/js/my-script.js',
        'assets/plugins/global/plugins.bundle.js',
        'assets/plugins/custom/prismjs/prismjs.bundle.js',
        'assets/js/scripts.bundle.js',
        'assets/plugins/custom/fullcalendar/fullcalendar.bundle.js',
        'assets/js/pages/widgets.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
//        'yii\bootstrap4\BootstrapPluginAsset'
    ];
}
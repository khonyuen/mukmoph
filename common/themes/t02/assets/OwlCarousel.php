<?php


namespace t02\assets;


use kartik\base\AssetBundle;

class OwlCarousel extends AssetBundle
{
    public $sourcePath = '@t02/dist/assets/plugins/custom/OwlCarousel/dist';
    public $css =[
        'assets/owl.carousel.min.css',
        'assets/owl.theme.default.min.css'
    ];

    public $js = [
        'owl.carousel.min.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];
}
<?php
namespace common\components;

use yii\base\Component;

class WebHelper extends Component
{

    public static function getUserName()
    {

        if (!\Yii::$app->user->isGuest) {
            return \Yii::$app->user->identity->getId();
        } else {
            return 'guest';
        }
    }

    public static function setAlert($key, $value)
    {
        return \Yii::$app->session->setFlash($key, $value);
    }

    public static function getOfficeName($off_id)
    {
        // CONCAT(o.off_id, ' ' ,o.`names`) as amphurname
        $sql = "SELECT
                CONCAT(o.`names`) as amphurname
                FROM
                office AS o
                WHERE
                o.off_id = :amphur limit 1";

        $db = \Yii::$app->db2;

        $rs = $db->createCommand($sql, [':amphur' => $off_id])->queryAll();
        //print_r($rs);

        return $rs[0]['amphurname'];
    }

    public static function getMeetingType(){

    }
}
<?php

namespace common\models;

use Yii;
use \common\models\base\MeetingType as BaseMeetingType;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "mt_meeting_type".
 */
class MeetingType extends BaseMeetingType
{
    const enable = 1;
    const disable = 0;
    /**
     * @inheritdoc
     */
   /* public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['meeting_type_name', 'meeting_type_status'], 'required'],
            [['meeting_type_status'], 'integer'],
            [['meeting_type_name'], 'string', 'max' => 255]
        ]);
    }*/

   public static function getMeetingType($isList=true){
       $mt = self::find()->select([
           'meeting_type_id','meeting_type_name'
       ])->where(['meeting_type_status'=>self::enable])->all();

       if($isList){
           $list = ArrayHelper::map($mt,'meeting_type_id','meeting_type_name');
       }else{
           $list = $mt;
       }
       return $list;
   }
	
}

<?php

namespace common\models;

use Yii;
use \common\models\base\SubAgendaFile as BaseSubAgendaFile;

/**
 * This is the model class for table "mt_sub_agenda_file".
 */
class SubAgendaFile extends BaseSubAgendaFile
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['sub_agenda_id'], 'required'],
            [['sub_agenda_id', 'created_user', 'updated_user'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['file_name', 'file_path'], 'string', 'max' => 255]
        ]);
    }
	
}

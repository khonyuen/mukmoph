<?php

namespace common\models;

use common\models\base\TopicType as BaseTopicType;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "mt_topic_type".
 */
class TopicType extends BaseTopicType
{
    /**
     * @inheritdoc
     */
    /*public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['topic_name', 'topic_order', 'topic_level'], 'required'],
            [['topic_order', 'topic_level'], 'integer'],
            [['topic_name'], 'string', 'max' => 255],
            [['topic_name'], 'unique'],
            [['topic_order'], 'unique']
        ]);
    }*/

    public static function getTopicList($isList = true)
    {
        $model = self::find()->select([
            'topic_id', 'topic_name', 'topic_order', 'topic_level',
        ])->orderBy('topic_id desc')->asArray()->all(); //->where(['meeting_type_status'=>self::enable])->all();

        if ($isList) {
            $list = ArrayHelper::map($model, function ($model) {
                return $model['topic_id'] . '|' . $model['topic_order'] . '|' . $model['topic_level'] . '|' . $model['topic_name'];
            }, 'topic_name');
            //print_r($list);
        } else {
            $list = $model;
        }
        return $list;
    }

    public static function getTopicListUpdate($isList = true, $meeting_id)
    {
        $model = self::find()->select([
            'mt_topic_type.topic_id',
	'mt_topic_type.topic_name',
	new Expression('if(mt_agenda_topic.topic_id is NULL,mt_agenda_topic.meeting_id,mt_agenda_topic.meeting_id) as `meeting_id`'),
	new Expression('if(mt_agenda_topic.topic_id is NULL,mt_topic_type.topic_order,mt_agenda_topic.`order`) as `order`'),
	new Expression('if(mt_agenda_topic.topic_id is NULL,mt_topic_type.topic_level,mt_agenda_topic.`level`) as `level`')
        ])
            ->leftJoin('mt_agenda_topic','mt_agenda_topic.topic_id=mt_topic_type.topic_id and mt_agenda_topic.meeting_id = :meeting_id',[':meeting_id'=>$meeting_id])
            //->where(['meeting_id'=>$meeting_id])
            ->orderBy('mt_topic_type.topic_id desc')->asArray()->all();

        if ($isList) {
            $list = ArrayHelper::map($model, function ($model) use ($meeting_id) {
                return $model['topic_id'] . '|' . $meeting_id. '|' . $model['order']. '|' . $model['level'];
            }, 'topic_name');
        } else {
            $list = $model;
        }
        return $list;
    }

}

<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[TopicType]].
 *
 * @see TopicType
 */
class TopicTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TopicType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TopicType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

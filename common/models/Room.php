<?php

namespace common\models;

use Yii;
use \common\models\base\Room as BaseRoom;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "mt_room".
 */
class Room extends BaseRoom
{
    const enable = 1;
    const disable = 0;
    /**
     * @inheritdoc
     */
    /*public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['room_detail'], 'string'],
            [['room_status'], 'integer'],
            [['room_name', 'room_icon'], 'string', 'max' => 255]
        ]);
    }*/
	public static function getRoomList($isList=true){
        $mt = self::find()->select([
            'room_id','room_name'
        ])->where(['room_status'=>self::enable])->all();

        if($isList){
            $list = ArrayHelper::map($mt,'room_id','room_name');
        }else{
            $list = $mt;
        }
        return $list;
    }
}

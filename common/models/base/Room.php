<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "mt_room".
 *
 * @property integer $room_id
 * @property string $room_name
 * @property string $room_detail
 * @property integer $room_status
 * @property string $room_icon
 *
 * @property \common\models\MtMeeting[] $mtMeetings
 */
class Room extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'mtMeetings'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['room_detail'], 'string'],
            [['room_status'], 'integer'],
            [['room_name', 'room_icon'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mt_room';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'room_id' => 'Room ID',
            'room_name' => 'ห้องประชุม',
            'room_detail' => 'รายละเอียด',
            'room_status' => 'Room Status',
            'room_icon' => 'Room Icon',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMtMeetings()
    {
        return $this->hasMany(\common\models\Meeting::className(), ['meeting_room' => 'room_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    /*public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_user',
                'updatedByAttribute' => 'updated_user',
            ],
        ];
    }*/


    /**
     * @inheritdoc
     * @return \common\models\RoomQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\RoomQuery(get_called_class());
    }
}

<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "mt_sub_agenda_file".
 *
 * @property integer $file_id
 * @property integer $sub_agenda_id
 * @property string $file_name
 * @property string $file_path
 * @property integer $created_user
 * @property integer $updated_user
 * @property string $created_at
 * @property string $updated_at
 *
 * @property \common\models\MtSubAgenda $subAgenda
 */
class SubAgendaFile extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'subAgenda'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sub_agenda_id'], 'required'],
            [['sub_agenda_id', 'created_user', 'updated_user'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['file_name', 'file_path'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mt_sub_agenda_file';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'file_id' => 'File ID',
            'sub_agenda_id' => 'วาระย่อย',
            'file_name' => 'ไฟล์',
            'file_path' => 'ชื่อไฟล์ในระบบ',
            'created_user' => 'ผู้บันทึก',
            'updated_user' => 'ผู้แก้ไข',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubAgenda()
    {
        return $this->hasOne(\common\models\MtSubAgenda::className(), ['sub_agenda_id' => 'sub_agenda_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_user',
                'updatedByAttribute' => 'updated_user',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\SubAgendaFileQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\SubAgendaFileQuery(get_called_class());
    }
}

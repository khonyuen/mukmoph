<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "mt_agenda_topic".
 *
 * @property integer $agenda_topic_id
 * @property integer $topic_id
 * @property integer $meeting_id
 * @property integer $order
 * @property integer $level
 * @property integer $created_user
 * @property integer $updated_user
 * @property string $created_at
 * @property string $updated_at
 *
 * @property \common\models\TopicType $topic
 * @property \common\models\Meeting $meeting
 * @property \common\models\SubAgenda[] $mtSubAgendas
 * @property \common\models\Agenda[] $mtAgendas
 */
class AgendaTopic extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'topic',
            'meeting',
            'mtSubAgendas',
            'mtAgendas',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['topic_id', 'meeting_id', 'order'], 'required'],
            [['topic_id', 'meeting_id', 'order', 'created_user', 'updated_user','level'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['topic_id', 'meeting_id'], 'unique', 'targetAttribute' => ['topic_id', 'meeting_id'], 'message' => 'ข้อมูลซ้ำ'],
            [['order','meeting_id'], 'unique','targetAttribute' => ['order','meeting_id'],'message' => 'ลำดับซ้ำ']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mt_agenda_topic';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'agenda_topic_id' => 'Agenda Topic ID',
            'topic_id' => 'Topic ID',
            'meeting_id' => 'Meeting ID',
            'order' => 'Order',
            'level' => 'Order',
            'created_user' => 'Created User',
            'updated_user' => 'Updated User',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopic()
    {
        return $this->hasOne(\common\models\TopicType::className(), ['topic_id' => 'topic_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeeting()
    {
        return $this->hasOne(\common\models\Meeting::className(), ['meeting_id' => 'meeting_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMtAgendas()
    {
        return $this->hasMany(\common\models\Agenda::className(), ['agenda_topic_id' => 'agenda_topic_id'])->orderBy('mt_agenda.agenda_order asc');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMtSubAgendas()
    {
        return $this->hasMany(\common\models\SubAgenda::className(), ['agenda_topic_id' => 'agenda_topic_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_user',
                'updatedByAttribute' => 'updated_user',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\AgendaTopicQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\AgendaTopicQuery(get_called_class());
    }
}

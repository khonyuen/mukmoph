<?php

namespace common\models\base;

use common\models\BaseActiveRecord;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "mt_meeting".
 *
 * @property integer $meeting_id
 * @property integer $meeting_type
 * @property string $meeting_month
 * @property string $meeting_year
 * @property integer $meeting_room
 * @property string $meeting_number
 * @property string $meeting_time
 * @property string $meeting_date
 * @property integer $meeting_status
 * @property integer $created_user
 * @property integer $updated_user
 * @property string $created_at
 * @property string $updated_at
 *
 * @property \common\models\Agenda[] $mtAgendas
 * @property \common\models\AgendaTopic[] $mtAgendaTopics
 * @property \common\models\MeetingType $meetingType
 * @property \common\models\Room $meetingRoom
 */
class Meeting extends BaseActiveRecord
{
    //use \mootensai\relation\RelationTrait;

    public $_agenda_topic;

    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'mtAgendas',
            'mtAgendaTopics',
            'meetingType'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['meeting_type', 'meeting_number', 'meeting_time', 'meeting_date', 'meeting_status','meeting_month','meeting_year','meeting_room'], 'required'],
            [['meeting_type', 'meeting_status', 'created_user', 'updated_user','meeting_room'], 'integer'],
            [['meeting_time', 'meeting_date', 'created_at', 'updated_at'], 'safe'],
            [['meeting_number'], 'string', 'max' => 255],
            [['meeting_month'], 'string', 'max' => 30],
            [['meeting_year'], 'string', 'max' => 10],
            ['_agenda_topic','required','message' => 'กรุณาเลือกวาระการประชุม'],

            [['meeting_type', 'meeting_number','meeting_month', 'meeting_year'], 'unique', 'targetAttribute' => ['meeting_number','meeting_type','meeting_month', 'meeting_year'], 'message' => 'ข้อมูลซ้ำ'],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mt_meeting';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'meeting_id' => 'Meeting ID',
            'meeting_type' => 'ประเภทการประชุม',
            'meeting_month' => 'ประชุมประจำเดือน',
            'meeting_year' => 'ปี พ.ศ.',
            'meeting_room' => 'ห้องประชุม',
            'meeting_number' => 'ประชุมครั้งที่',
            'meeting_time' => 'เวลา',
            'meeting_date' => 'วันที่ประชุม',
            'meeting_status' => 'สถานะ',
            'created_user' => 'ผู้บันทึก',
            'updated_user' => 'ผู้แก้ไข',
            '_agenda_topic'=>'เลือกวาระการประชุมที่ต้องการ'
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMtAgendas()
    {
        return $this->hasMany(\common\models\Agenda::className(), ['meeting_id' => 'meeting_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMtAgendaTopics()
    {
        return $this->hasMany(\common\models\AgendaTopic::className(), ['meeting_id' => 'meeting_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeetingType()
    {
        return $this->hasOne(\common\models\MeetingType::className(), ['meeting_type_id' => 'meeting_type']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeetingRoom()
    {
        return $this->hasOne(\common\models\Room::className(), ['room_id' => 'meeting_room']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_user',
                'updatedByAttribute' => 'updated_user',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\MeetingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\MeetingQuery(get_called_class());
    }
}

<?php

namespace common\models\base;

use common\models\BaseActiveRecord;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "mt_agenda".
 *
 * @property integer $agenda_id
 * @property integer $meeting_id
 * @property string $agenda_name
 * @property string $agenda_files
 * @property string $agenda_files_tmp
 * @property integer $agenda_topic_type
 * @property integer $agenda_topic_id
 * @property integer $agenda_status
 * @property integer $agenda_order
 * @property integer $created_user
 * @property integer $updated_user
 * @property string $created_at
 * @property string $updated_at
 *
 * @property \common\models\Meeting $meeting
 * @property \common\models\TopicType $agendaTopicType
 * @property \common\models\SubAgenda[] $mtSubAgendas
 * @property \common\models\AgendaTopic $agendaTopic
 */
class Agenda extends BaseActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'meeting',
            'agendaTopicType',
            'mtSubAgendas',
            'agendaTopic',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['meeting_id', 'agenda_name', 'agenda_topic_type', 'agenda_status','agenda_topic_id','agenda_order'], 'required'],
            [['meeting_id', 'agenda_topic_type', 'agenda_status', 'agenda_order', 'created_user', 'updated_user','agenda_topic_id'], 'integer'],
            [['agenda_name'], 'string'],
            [['agenda_files'], 'file', 'extensions' => ['jpg', 'png', 'gif', 'bmp','pdf','xls','xlsx','doc','docx','ppt','pptx','txt'], 'maxFiles' => 20, 'skipOnEmpty' => true,],
            [['created_at', 'updated_at'], 'safe'],
            [['agenda_files_tmp'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mt_agenda';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'agenda_id' => 'Agenda ID',
            'meeting_id' => 'หัวข้อประชุม',
            'agenda_name' => 'รายละเอียดวาระ',
            'agenda_files' => 'เอกสารแนบ',
            'agenda_files_tmp' => 'Agenda Files Tmp',
            'agenda_topic_type' => 'ระเบียบวาระที่',
            'agenda_topic_id' => 'ประเภท+การประชุม',
            'agenda_status' => 'สถานะ',
            'agenda_order' => 'การเรียงลำดับ',
            'created_user' => 'ผู้บันทึก',
            'updated_user' => 'ผู้แก้ไข',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMeeting()
    {
        return $this->hasOne(\common\models\Meeting::className(), ['meeting_id' => 'meeting_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgendaTopicType()
    {
        return $this->hasOne(\common\models\TopicType::className(), ['topic_id' => 'agenda_topic_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgendaTopic()
    {
        return $this->hasOne(\common\models\AgendaTopic::className(), ['agenda_topic_id' => 'agenda_topic_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMtSubAgendas()
    {
        return $this->hasMany(\common\models\SubAgenda::className(), ['agenda_id' => 'agenda_id'])->orderBy('mt_sub_agenda.sub_order asc');
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_user',
                'updatedByAttribute' => 'updated_user',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\AgendaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\AgendaQuery(get_called_class());
    }
}

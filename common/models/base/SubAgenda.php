<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "mt_sub_agenda".
 *
 * @property integer $sub_agenda_id
 * @property integer $agenda_id
 * @property integer $agenda_topic_id
 * @property string $detail
 * @property integer $sub_order
 * @property string $sub_files
 * @property string $sub_files_tmp
 * @property integer $created_user
 * @property integer $updated_user
 * @property string $created_at
 * @property string $updated_at
 *
 * @property \common\models\Agenda $agenda
 * @property \common\models\AgendaTopic $agendaTopic
 * @property \common\models\SubAgendaFile[] $mtSubAgendaFiles
 */
class SubAgenda extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'agenda',
            'agendaTopic',
            'mtSubAgendaFiles'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['agenda_id', 'detail','sub_order'], 'required'],
            [['agenda_id', 'agenda_topic_id', 'sub_order', 'created_user', 'updated_user'], 'integer'],
            [['detail'], 'string'],
            [['sub_files'], 'file', 'extensions' => ['jpg', 'png', 'gif', 'bmp','pdf','xls','xlsx','doc','docx','ppt','pptx','txt'], 'maxFiles' => 20, 'skipOnEmpty' => true,],
            [['created_at', 'updated_at'], 'safe'],
            [['sub_files_tmp'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mt_sub_agenda';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sub_agenda_id' => 'Sub Agenda ID',
            'agenda_id' => 'Agenda ID',
            'agenda_topic_id' => 'Agenda Topic ID',
            'detail' => 'รายละเอียด',
            'sub_order' => 'เรียง',
            'sub_files' => 'ไฟล์แนบ',
            'sub_files_tmp' => 'ตำแหน่ง',
            'created_user' => 'ผู้บันทึก',
            'updated_user' => 'ผู้แก้ไข',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgenda()
    {
        return $this->hasOne(\common\models\Agenda::className(), ['agenda_id' => 'agenda_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgendaTopic()
    {
        return $this->hasOne(\common\models\AgendaTopic::className(), ['agenda_topic_id' => 'agenda_topic_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMtSubAgendaFiles()
    {
        return $this->hasMany(\common\models\SubAgendaFile::className(), ['sub_agenda_id' => 'sub_agenda_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_user',
                'updatedByAttribute' => 'updated_user',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\SubAgendaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\SubAgendaQuery(get_called_class());
    }
}

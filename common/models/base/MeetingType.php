<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "mt_meeting_type".
 *
 * @property integer $meeting_type_id
 * @property string $meeting_type_name
 * @property integer $meeting_type_status
 * @property string $meeting_type_detail
 *
 * @property \common\models\Meeting[] $mtMeetings
 */
class MeetingType extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'mtMeetings'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['meeting_type_name', 'meeting_type_status'], 'required'],
            [['meeting_type_status'], 'integer'],
            [['meeting_type_name','meeting_type_detail'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mt_meeting_type';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'meeting_type_id' => 'Meeting Type ID',
            'meeting_type_name' => 'Meeting Type Name',
            'meeting_type_detail' => 'รายละเอียด',
            'meeting_type_status' => 'Meeting Type Status',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMtMeetings()
    {
        return $this->hasMany(\common\models\Meeting::className(), ['meeting_type' => 'meeting_type_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_user',
                'updatedByAttribute' => 'updated_user',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\MeetingTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\MeetingTypeQuery(get_called_class());
    }
}

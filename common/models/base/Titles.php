<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "titles".
 *
 * @property string $TitleCode
 * @property string $TName
 * @property string $Sex
 *
 * @property \common\models\User[] $users
 */
class Titles extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'users'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TitleCode'], 'required'],
            [['TitleCode'], 'string', 'max' => 3],
            [['TName', 'Sex'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'titles';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'TitleCode' => 'รหัส',
            'TName' => 'คำนำหน้า',
            'Sex' => 'เพศ',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(\common\models\User::className(), ['title' => 'TitleCode']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_user',
                'updatedByAttribute' => 'updated_user',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\TitlesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\TitlesQuery(get_called_class());
    }
}

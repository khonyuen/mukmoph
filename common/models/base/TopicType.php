<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "mt_topic_type".
 *
 * @property integer $topic_id
 * @property string $topic_name
 * @property integer $topic_order
 * @property integer $topic_level
 *
 * @property \common\models\Agenda[] $mtAgendas
 * @property \common\models\AgendaTopic[] $mtAgendaTopics
 */
class TopicType extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'mtAgendas',
            'mtAgendaTopics'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['topic_name', 'topic_order', 'topic_level'], 'required'],
            [['topic_order', 'topic_level'], 'integer'],
            [['topic_name'], 'string', 'max' => 255],
            [['topic_name'], 'unique'],
            [['topic_order'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mt_topic_type';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'topic_id' => 'Topic ID',
            'topic_name' => 'วาระ',
            'topic_order' => 'การเรียงลำดับ',
            'topic_level' => '1 วาระหลัก 2 วาระย่อย',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMtAgendas()
    {
        return $this->hasMany(\common\models\Agenda::className(), ['agenda_topic_type' => 'topic_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMtAgendaTopics()
    {
        return $this->hasMany(\common\models\AgendaTopic::className(), ['topic_id' => 'topic_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_user',
                'updatedByAttribute' => 'updated_user',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\TopicTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\TopicTypeQuery(get_called_class());
    }
}

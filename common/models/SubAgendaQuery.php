<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[SubAgenda]].
 *
 * @see SubAgenda
 */
class SubAgendaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SubAgenda[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SubAgenda|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

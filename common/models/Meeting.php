<?php

namespace common\models;

use Yii;
use \common\models\base\Meeting as BaseMeeting;

/**
 * This is the model class for table "mt_meeting".
 */
class Meeting extends BaseMeeting
{

    /**
     * @inheritdoc
     */
    /*public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['meeting_type', 'meeting_number', 'meeting_time', 'meeting_date', 'meeting_status'], 'required'],
            [['meeting_type', 'meeting_status', 'created_user', 'updated_user'], 'integer'],
            [['meeting_time', 'meeting_date', 'created_at', 'updated_at'], 'safe'],
            [['meeting_number'], 'string', 'max' => 255]
        ]);
    }*/

    function formatMeetingDate(){
        if($this->meeting_date=='0000-00-00'){
            return "-";
        }
        $monthTh = self::getMonthInThai($this->meeting_month);
        $date = explode('-',$this->meeting_date);
        return $date[2].' '.$monthTh.' '.($date[0]+543);
    }

    function setFormatMeetingDatePicker(){
        if($this->meeting_date=='0000-00-00'){
            return "";
        }
        $date = explode('-',$this->meeting_date);
        $this->meeting_date = $date[2].'-'.$date[1].'-'.($date[0]);
    }

    function setFormatMeetingDateDb(){
        if($this->meeting_date=='0000-00-00'){
            return $this->meeting_date;
        }
        $date = explode('-',$this->meeting_date);
        $this->meeting_date = $date[2].'-'.$date[1].'-'.($date[0]);
    }

    function meetingThaiMonth(){
        return self::getMonthInThai($this->meeting_month);
    }

    function formatMeetingTime(){
        $time = substr($this->meeting_time,0,5);
        echo  $time;
    }

    function setFormatMeetingTime(){
        $time = substr($this->meeting_time,0,5);
        return  $this->meeting_time = $time;
    }

}

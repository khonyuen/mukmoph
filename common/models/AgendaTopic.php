<?php

namespace common\models;

use Yii;
use \common\models\base\AgendaTopic as BaseAgendaTopic;
use yii\db\Expression;

/**
 * This is the model class for table "mt_agenda_topic".
 */
class AgendaTopic extends BaseAgendaTopic
{
    /**
     * @inheritdoc
     */
    /*public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['topic_id', 'meeting_id', 'order'], 'required'],
            [['topic_id', 'meeting_id', 'order', 'created_user', 'updated_user'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['topic_id', 'meeting_id'], 'unique', 'targetAttribute' => ['topic_id', 'meeting_id'], 'message' => 'The combination of Topic ID and Meeting ID has already been taken.'],
            [['order'], 'unique']
        ]);
    }*/

    public static function createModel($meeting_id, $md){
        // $model['topic_id'].'|'.$model['topic_order'].'|'.$model['topic_level'].'|'.$model['topic_name'];
        //print_r($md);

        if(is_array($md)){
            $models = [];
            foreach ($md as $item) {
                $array = explode('|',$item);
                $models[] = [$array[0],$meeting_id,$array[1],$array[2],Yii::$app->user->id,new Expression('NOW()')];
            }

            if($models!=[]){
                $command = Yii::$app->db->createCommand();
                $command->batchInsert(self::tableName(),
                    ['topic_id', 'meeting_id', 'order','level','created_user','created_at'],
                    $models
                );
                return $command->execute();
            }
        }else{
            return;
        }
    }
}
